#ifndef SKINNINGMANAGER_H
#define SKINNINGMANAGER_H

#include "trimesh/drawable_trimesh.h"
#include "boundedbiharmonicweights.h"
#include "boneheatweights.h"
#include "noneweights.h"
#include "euclideandistanceweights.h"
#include "linearblendskinning.h"
#include "dualquaternionskinning.h"
#include "Eigen/Dense"
#include "Eigen/Sparse"
#include "skel/import.h"
#include "utils/utils.h"
#include "matrixmanipulation.h"

class SkinningManager
{
public:
    SkinningManager();
    SkinningManager(DrawableTrimesh *trimesh, Skel::CurveSkeleton *skeleton);

    void setTrimesh     (DrawableTrimesh *trimesh);
    void setHandleID    (int _id);
    void init ();
    void calculateWeights ();
    void applySkinning (Eigen::Matrix4d transformation, QList<int> handles);
    void applySkinning (qglviewer::Quaternion rotation, qglviewer::Frame * const frame, QList<int> handles);

    void setWeightMode (WeightsMode _mode);
    void setDeformMode (DeformationMode _mode);
    Eigen::SparseMatrix<double> getWeights (WeightsMode _mode);

    void saveWeights (const char *filename);
    void loadWeights (const char *filename);

private:
    Eigen::MatrixXd             meshMatrix;
    Eigen::MatrixXd             handlesMatrix;
    DrawableTrimesh             *mesh;
    Skel::CurveSkeleton         *skel;

    NoneWeights                 none;
    BoundedBiharmonicWeights    biharmonic;
    BoneHeatWeights             boneHeat;
    EuclideanDistanceWeights    euclidean;
    LinearBlendSkinning         lbs;
    DualQuaternionSkinning      dqs;

    WeightsMode                 wMode;
    DeformationMode             dMode;
    int                         handleId;
    bool                        boneHeatCalculated;
    bool                        biharmonicCalculated;

    void handleToMatrix     (Skel::CurveSkeleton *skeleton);

};

#endif // SKINNINGMANAGER_H
