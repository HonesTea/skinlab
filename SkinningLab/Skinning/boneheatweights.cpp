#include "boneheatweights.h"

BoneHeatWeights::BoneHeatWeights()
{

}

BoneHeatWeights::BoneHeatWeights(Eigen::MatrixXd *handles, Eigen::MatrixXd *mesh)
{
    handlesMatrix = handles;
    meshMatrix = mesh;
}

BoneHeatWeights::BoneHeatWeights(const std::vector<Eigen::Triplet<double> > tripletList, int rows, int cols)
{
    weights = Eigen::SparseMatrix<double> (rows, cols);
    weights.setFromTriplets(tripletList.begin(), tripletList.end());
}

void BoneHeatWeights::setDrawableTrimesh(DrawableTrimesh *mesh)
{
    trimesh = mesh;
}

Eigen::SparseMatrix<double> BoneHeatWeights::getWeights()
{
    return weights;
}

// The bone-heat weights are calculated trough this formula
//          (L+K)W = KE
// where:
//          L is discrite laplacian matrix nxn
//          K is a diagonal matrix nxn, which measure confidence
//          E is Euclidean matrix of proximity
void BoneHeatWeights::computeWeights()
{
    Eigen::SparseMatrix<double> b;
    typedef Eigen::Triplet<double> T;
    std::vector<T> tripletList;
    cout << meshMatrix->rows() << endl;
    tripletList.reserve(meshMatrix->rows() *2);



    weights = Eigen::SparseMatrix<double, Eigen::ColMajor>(meshMatrix->rows(), handlesMatrix->rows());

    Eigen::SparseMatrix<double> laplacian;
    Eigen::SparseMatrix<double> euclidean;
    Eigen::SparseMatrix<double> confidence;
    Eigen::SparseMatrix<double> massInv;

    calculateEuclidean(&euclidean);
    calculateConfidence(&confidence, &euclidean);
    calculateLaplacian(&laplacian);
    calculateMassMatrix(&massInv);

    for(int i = 0; i < meshMatrix->rows(); ++i)
    {
        double bi = confidence.coeffRef(i,i) / massInv.coeffRef(i,i);
        laplacian.coeffRef(i,i) +=  bi;
        tripletList.push_back(T(i, i, bi));
    }

    b = Eigen::SparseMatrix<double> (meshMatrix->rows(),meshMatrix->rows());
    b.setZero();
    b.setFromTriplets(tripletList.begin(), tripletList.end());
    b.makeCompressed();

    b = b*euclidean;

    Eigen::MatrixXd inverseLaplacian = Eigen::MatrixXd(laplacian);
    inverseLaplacian.inverse();
    laplacian = inverseLaplacian.sparseView();

    Eigen::SimplicialLLT<Eigen::SparseMatrix<double>> solver;
    solver.compute(laplacian);
    weights = solver.solve(b);

    weights.makeCompressed();

}

//the euclidean matrix values are:
//               {1 if d(v,hi) < d(v,hj) for any i!=j
// w i,j (v) =   {
//               {0 otherwise
void BoneHeatWeights::calculateEuclidean(Eigen::SparseMatrix<double> *euclidean)
{
    double distance;
    double distance2;
    int nearest = 0;

    typedef Eigen::Triplet<double> T;
    std::vector<T> tripletList;
    tripletList.reserve(meshMatrix->rows() *10);

    for(int i = 0; i < meshMatrix->rows(); ++i)
    {
        distance = (meshMatrix->row(i) - handlesMatrix->row(0)).squaredNorm();
        nearest = 0;
        int k = 0;

        for(int j = 0; j < handlesMatrix->rows(); ++j)
        {
            distance2 = (meshMatrix->row(i) - handlesMatrix->row(j)).squaredNorm();

            if(distance > distance2)
            {
                nearest = j;
                distance = distance2;
            }
        }

        tripletList.push_back(T(i, nearest, 1));
    }

    *euclidean = Eigen::SparseMatrix<double> (meshMatrix->rows(),handlesMatrix->rows());
    euclidean->setZero();
    euclidean->setFromTriplets(tripletList.begin(), tripletList.end());
    euclidean->makeCompressed();
}

//   confidence matrix is a diagonal matrix where:
//          { 0 if vi is not visible from the nearest handle
//   cii =  {
//          { d(v,h)^-2 otherwise
void BoneHeatWeights::calculateConfidence(Eigen::SparseMatrix<double> *confidence, Eigen::SparseMatrix<double> *euclidean)
{
    typedef Eigen::Triplet<double> T;
    std::vector<T> tripletList;

    //v1,v2,v3 defines a plane
    //la,lb define a line
    Eigen::Vector3d v1;
    Eigen::Vector3d v2;
    Eigen::Vector3d v3;
    Eigen::Vector3d handle;
    Eigen::Vector3d vertex;

    tripletList.reserve(meshMatrix->rows());

    for (int k=0; k<euclidean->outerSize(); ++k)
    {
        for (Eigen::SparseMatrix<double>::InnerIterator it(*euclidean,k); it; ++it)
        {
            bool intersect = false;
            int i = 0;

            handle = handlesMatrix->row(it.col());
            vertex = meshMatrix->row(it.row());

            /// i should create an octree but in the meanwhile i'll just comment this whole section
//            while (!intersect && (i < trimesh.num_triangles()))
//            {
//                Eigen::Vector3d x;
//                Eigen::Vector3d b;
//                Eigen::Matrix3d A;

//                vector<int> triangleVertices = trimesh.getTriangle(i);
//                i++;

//                v1 = meshMatrix.row(triangleVertices[0]);
//                v2 = meshMatrix.row(triangleVertices[1]);
//                v3 = meshMatrix.row(triangleVertices[2]);

////                intersect = triangle_intersection(v1,v2,v3,handle,vertex);

//                /// the calculus isn't so expensive, the while statement itself take around 26s on the hand mesh.
//                /// calculus take 2 seconds more
//                A << handle(0) - vertex(0), v2(0) - v1(0), v3(0) - v1(0),
//                     handle(1) - vertex(1), v2(1) - v1(1), v3(1) - v1(1),
//                     handle(2) - vertex(2), v2(2) - v1(2), v3(2) - v1(2);

//                b << handle(0) - v1(0),
//                     handle(1) - v1(1),
//                     handle(2) - v1(2);

//                x = A.ldlt().solve(b);

//                if(x(0) < 1 && x(0) >0 && x(1)>0 && x(2)>0 && ((x(1) + x(2) < 1) || (x(1) + x(2) == 0)))
//                {
//                    intersect = true;
//                }
//            }


            if(!intersect)
            {
                tripletList.push_back(T(it.row(), it.row(), 1/((vertex - handle).squaredNorm())));
            }
        }
    }

    *confidence = Eigen::SparseMatrix<double> (meshMatrix->rows(), meshMatrix->rows());
    confidence->setZero();
    confidence->setFromTriplets(tripletList.begin(), tripletList.end());
    confidence->makeCompressed();
}

// the laplacian matrix is an nxn matrix where:
//          { -1/2 (cot(aij) + cot(bij)              if i!=j and vi adjacent vj,
//          {                                       where aij and bij are the opposite vertices to the edgeij
// Li,j =   { -sum of other values on the line      if i=j
//          {  0                                    otherwise
void BoneHeatWeights::calculateLaplacian(Eigen::SparseMatrix<double> *laplacian)
{
    vector<vector<int>> adjacencyMatrix = trimesh->getVerticesAdjacency();

    typedef Eigen::Triplet<double> T;
    std::vector<T> tripletList;
    tripletList.reserve(meshMatrix->rows() *10);

    Eigen::Vector3d v1;
    Eigen::Vector3d v2;
    Eigen::Vector3d v3;

    Eigen::Vector3d edge1;
    Eigen::Vector3d edge2;

    double cotangents;

    for(int i = 0; i < trimesh->num_triangles(); ++i)
    {
        vector<int> triangle = trimesh->getTriangle(i);

        for(int j = 0; j < 3; ++j)
        {
            int v1id = triangle[j];
            int v2id = triangle[(j +1)%3];
            int v3id = triangle[(j +2)%3];

            v1(0) = trimesh->vector_coords().at(v1id *3   );
            v1(1) = trimesh->vector_coords().at(v1id *3 +1);
            v1(2) = trimesh->vector_coords().at(v1id *3 +2);

            v2(0) = trimesh->vector_coords().at(v2id *3   );
            v2(1) = trimesh->vector_coords().at(v2id *3 +1);
            v2(2) = trimesh->vector_coords().at(v2id *3 +2);

            v3(0) = trimesh->vector_coords().at(v3id *3   );
            v3(1) = trimesh->vector_coords().at(v3id *3 +1);
            v3(2) = trimesh->vector_coords().at(v3id *3 +2);

            edge1 = v1 - v3;
            edge2 = v2 - v3;
            edge1 = edge1 / edge1.norm();
            edge2 = edge2 / edge2.norm();
            cotangents = ((edge1.dot(edge2)) / (edge1.cross(edge2).norm()))/4;

            tripletList.push_back(T(v2id, v1id, -cotangents));
            tripletList.push_back(T(v1id, v2id, -cotangents));
            tripletList.push_back(T(v1id, v1id, cotangents));
            tripletList.push_back(T(v2id, v2id, cotangents));
        }
    }

    *laplacian = Eigen::SparseMatrix<double> (meshMatrix->rows(), meshMatrix->rows());
    laplacian->setZero();
    laplacian->setFromTriplets(tripletList.begin(), tripletList.end());
    laplacian->makeCompressed();
}

void BoneHeatWeights::calculateMassMatrix(Eigen::SparseMatrix<double> *massInv)
{
    Eigen::Vector3d v1;
    Eigen::Vector3d v2;
    Eigen::Vector3d v3;
    vector<vector<int>> adjacencyMatrix = trimesh->getVerticesAdjacency();
    typedef Eigen::Triplet<double> T;
    std::vector<T> tripletList;
    tripletList.reserve(meshMatrix->rows() *2);

    for(int i = 0; i < trimesh->num_triangles(); ++i)
    {
        vector<int> triangle = trimesh->getTriangle(i);


            int v1id = triangle[0];
            int v2id = triangle[1];
            int v3id = triangle[2];

            v1(0) = trimesh->vector_coords().at(v1id *3   );
            v1(1) = trimesh->vector_coords().at(v1id *3 +1);
            v1(2) = trimesh->vector_coords().at(v1id *3 +2);

            v2(0) = trimesh->vector_coords().at(v2id *3   );
            v2(1) = trimesh->vector_coords().at(v2id *3 +1);
            v2(2) = trimesh->vector_coords().at(v2id *3 +2);

            v3(0) = trimesh->vector_coords().at(v3id *3   );
            v3(1) = trimesh->vector_coords().at(v3id *3 +1);
            v3(2) = trimesh->vector_coords().at(v3id *3 +2);

            double area = ((v2 - v1).cross(v3 - v1)).norm() /2;;

            tripletList.push_back(T(v1id, v1id, area));
            tripletList.push_back(T(v2id, v2id, area));
            tripletList.push_back(T(v3id, v3id, area));
    }

//    for(int i = 0; i < trimesh.num_vertices(); ++i)
//    {
//        double sum = 0;

//        v1 = Eigen::Vector3d(trimesh.vector_coords().at(i*3),
//                             trimesh.vector_coords().at(i*3 +1),
//                             trimesh.vector_coords().at(i*3 +2));

//        for (int neigh : adjacencyMatrix[i])
//        {

//            v2 = Eigen::Vector3d(trimesh.vector_coords().at(neigh*3),
//                                 trimesh.vector_coords().at(neigh*3 +1),
//                                 trimesh.vector_coords().at(neigh*3 +2));

//            for(int neighOfNeigh : adjacencyMatrix[neigh])
//            {
//                if(neighOfNeigh!=i)
//                {
//                    v3 = Eigen::Vector3d(trimesh.vector_coords().at(neighOfNeigh*3),
//                                         trimesh.vector_coords().at(neighOfNeigh*3 +1),
//                                         trimesh.vector_coords().at(neighOfNeigh*3 +2));

//                    sum += ((v2 - v1).cross(v3 - v1)).norm() /2;
//                }
//            }
//        }
//        tripletList.push_back(T(i, i, 1/sum));
//    }

    *massInv = Eigen::SparseMatrix<double> (meshMatrix->rows(), meshMatrix->rows());
    massInv->setZero();
    massInv->setFromTriplets(tripletList.begin(), tripletList.end());

    for(int i = 0; i < massInv->rows(); ++i)
    {
        massInv->coeffRef(i,i) = 1/massInv->coeffRef(i,i);
    }

    massInv->makeCompressed();
}

//http://www.cs.virginia.edu/~gfx/Courses/2003/ImageSynthesis/papers/Acceleration/Fast%20MinimumStorage%20RayTriangle%20Intersection.pdf
bool BoneHeatWeights::triangle_intersection( const Eigen::Vector3d  v1,
                                             const Eigen::Vector3d  v2,
                                             const Eigen::Vector3d  v3,
                                             const Eigen::Vector3d  origin,
                                             const Eigen::Vector3d  end)
{
    Eigen::Vector3d edge1, edge2, tVec, pVec, qVec, direction;
    double det, invDet, u, v, t;

    edge1 = v2 - v1;
    edge2 = v3 - v1;
    direction = end - origin;

    pVec = direction.cross(edge2);
    det = edge1.dot(pVec);

    //if

//    if(det < EPSILON)
//        return 0;

//    tVec = origin - v1;

//    u = tVec.dot(pVec);

//    if(u < 0.0 || u > det)
//        return 0;

//    qVec = tVec.cross(edge1);

//    v = direction.dot(qVec);

//    if(v < 0.0 || u + v > det)
//        return 0;

//    t = edge2.dot(qVec);
//    invDet = 1.0 / det;
//    t *= invDet;
//    u *= invDet;
//    v *= invDet;

    //else

    if(det > -EPSILON && det < EPSILON)
        return false;

    invDet = 1.0 / det;

    tVec = origin - v1;
    u = tVec.dot(pVec) *invDet;

    if(u < 0.0 || u > 1.0)
        return false;

    qVec = tVec.cross(edge1);
    v = direction.dot(qVec) *invDet;

    if(v < 0.0 || u + v > det)
        return false;

    t = edge2.dot(qVec) *invDet;

    if(t > direction.norm())
        return false;

    return true;

}
