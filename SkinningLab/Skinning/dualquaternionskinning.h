#ifndef DUALQUATERNIONSKINNING_H
#define DUALQUATERNIONSKINNING_H

#include <QList>
#include "QGLViewer/quaternion.h"
#include "QGLViewer/frame.h"
#include "Eigen/Sparse"
#include "trimesh/drawable_trimesh.h"
#include "dualquaternion.h"
#include "skel/import.h"

class DualQuaternionSkinning
{
public:
    DualQuaternionSkinning();
    DualQuaternionSkinning(DrawableTrimesh* m);

    void applySkinning  (Eigen::Matrix4d transformation, QList<int> handles, Eigen::SparseMatrix<double> weights);
    void applySkinning  (qglviewer::Quaternion rotation, qglviewer::Frame * const frame, QList<int> handles, Eigen::SparseMatrix<double> weights);

    void setTrimesh(DrawableTrimesh* m);
    void setSkeleton(Skel::CurveSkeleton * _skel);
    void setHandlesMatrix(Eigen::MatrixXd _handles);
    void setMeshMatrix(Eigen::MatrixXd _mesh);
    void sethandleId(int _id);

private:
    DrawableTrimesh*    mesh;
    Skel::CurveSkeleton *skel;
    Eigen::MatrixXd     handlesMatrix;
    Eigen::MatrixXd     meshMatrix;
    int                 handleId;
};

#endif // DUALQUATERNIONSKINNING_H
