#ifndef SKINNINGWEIGHT_H
#define SKINNINGWEIGHT_H

#include "skel/import.h"
#include "trimesh/drawable_trimesh.h"

class SkinningWeight
{
public:
    SkinningWeight(){}

    virtual void computeWeights() =0;
};

#endif // SKINNINGWEIGHT_H
