#ifndef LINEARBLENDSKINNING_H
#define LINEARBLENDSKINNING_H

#include <QList>
#include "QGLViewer/quaternion.h"
#include "QGLViewer/frame.h"
#include "trimesh/drawable_trimesh.h"
#include "Eigen/Sparse"

class LinearBlendSkinning
{
public:
    LinearBlendSkinning ();
    LinearBlendSkinning (DrawableTrimesh* m);

    void applySkinning  (Eigen::Matrix4d transformation, QList<int> handles, Eigen::SparseMatrix<double> weights);
    void applySkinning  (qglviewer::Quaternion rotation, qglviewer::Frame * const frame, QList<int> handles, Eigen::SparseMatrix<double> weights);

    void setIdHandle    (int id);
    void setTrimesh     (DrawableTrimesh* m);

private:
    int                 idHandle;
    DrawableTrimesh*    mesh;

};

#endif // LINEARBLENDSKINNING_H
