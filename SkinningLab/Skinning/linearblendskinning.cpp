#include "linearblendskinning.h"

LinearBlendSkinning::LinearBlendSkinning()
{

}

LinearBlendSkinning::LinearBlendSkinning(DrawableTrimesh* m)
{
    mesh = m;
}

void LinearBlendSkinning::setTrimesh(DrawableTrimesh* m)
{
    mesh = m;
}

void LinearBlendSkinning::applySkinning(Eigen::Matrix4d transformation, QList<int> handles, Eigen::SparseMatrix<double> weights)
{
    vector<double> vertexCoords = mesh->vector_coords();
    Eigen::Matrix4d t;
    Eigen::Vector4d vertexVector;
    int previous = 0;
    double sum = 0;

//    t.setZero();

//    for (int k=0; k<weights.outerSize(); ++k)
//    {
//        for (Eigen::SparseMatrix<double>::InnerIterator it(weights,k); it; ++it)
//        {
//            if (previous != it.row())
//            {
//                vertexVector =  Eigen::Vector4d (vertexCoords[previous *3],
//                                                 vertexCoords[previous *3 +1],
//                                                 vertexCoords[previous *3 +2],
//                                                 1);
//                vertexVector = t * vertexVector;
//                vertexVector = vertexVector * (1/vertexVector(3));
//                t.setZero();
//                mesh->set_vertex(previous, vec3<double>(vertexVector(0),
//                                                        vertexVector(1),
//                                                        vertexVector(2)));
//                previous = it.row();
//            }

//            if(handles.contains(it.col()))
//            {
//                t += transformation * it.value();
//            }
//            else
//            {
//                t += Eigen::Matrix4d::Identity() * it.value();
//            }
//        }
//    }

    //working but slow
    for (int i = 0; i<mesh->num_vertices(); ++i)
    {
        t.setZero();
        sum = 0;

        for (int j = 0; j< handles.size(); ++j)
        {
            t += transformation * weights.coeffRef(i, handles.at(j));
            sum += weights.coeffRef(i, handles.at(j));
        }

        t += Eigen::Matrix4d::Identity() * (1-sum);

        if(!t.isZero(0))
        {
            vertexVector =  Eigen::Vector4d (vertexCoords[i *3],
                                             vertexCoords[i *3 +1],
                                             vertexCoords[i *3 +2],
                                             1);
            vertexVector = t * vertexVector;
            vertexVector = vertexVector * (1/vertexVector(3));

            mesh->set_vertex(i, vec3<double>(vertexVector(0),
                                             vertexVector(1),
                                             vertexVector(2)));
        }
    }
}

void LinearBlendSkinning::applySkinning(qglviewer::Quaternion rotation, qglviewer::Frame * const frame, QList<int> handles, Eigen::SparseMatrix<double> weights)
{
    double sumWeights = 0;
    int previousRow = 0;
    Eigen::VectorXd sumWeightsVector (mesh->num_vertices());
    vector<double> vertexCoords = mesh->vector_coords();

    const qglviewer::Vec worldAxis = frame->inverseTransformOf(rotation.axis());
    qglviewer::Vec pos = frame->position();
    const float angle = rotation.angle();

    //implementation trough Eigen sparsematrix iteration
//    for (int k=0; k<weights.outerSize(); ++k)
//    {
//        for (Eigen::SparseMatrix<double>::InnerIterator it(weights,k); it; ++it)
//        {
//            if(handles.contains(it.col()))
//            {
//                int row = it.row();
//                if((previousRow != it.row()) && (sumWeights > 0))
//                {
//                    qglviewer::Vec point(vertexCoords[previousRow*3],
//                                         vertexCoords[previousRow*3 +1],
//                                         vertexCoords[previousRow*3 +2]);
//                    qglviewer::Vec pointRotation = qWorld.rotate(point - pos);


//                    mesh->set_vertex(previousRow, vec3<double>(pos.x + pointRotation.x *sumWeights,
//                                                               pos.y + pointRotation.y *sumWeights,
//                                                               pos.z + pointRotation.z *sumWeights));
//                    sumWeights = 0;
//                    previousRow = it.row();
//                }
//                sumWeights += it.value();
//            }

//        }
//    }

    //working but slow
    for (int i = 0; i<mesh->num_vertices(); ++i)
    {

        for (int j = 0; j< handles.size(); ++j)
        {
              sumWeights += weights.coeffRef(i, handles.at(j));
        }

        qglviewer::Quaternion qWorld(worldAxis, angle * sumWeights);
        qglviewer::Vec point(vertexCoords[i*3], vertexCoords[i*3 +1], vertexCoords[i*3 +2]);
        qglviewer::Vec pointRotation = qWorld.rotate(point - pos);


        if(sumWeights > 0)
        {
            mesh->set_vertex(i, vec3<double>(pos.x + pointRotation.x,
                                             pos.y + pointRotation.y,
                                             pos.z + pointRotation.z));
        }

        sumWeights = 0;
    }

        ///conversion from quaternion to transformation
//    Eigen::Matrix4d t;
//    Eigen::Vector4d vertexVector;
//    int previous = 0;
//    double sum = 0;

//    qglviewer::Quaternion qWorld(worldAxis, -angle );

//    qreal q[3][3];
//    qWorld.getInverseRotationMatrix(q);

//    Eigen::Matrix4d transformation;

//    transformation << q[0][0], q[0][1], q[0][2], 0,
//                        q[1][0], q[1][1], q[1][2], 0,
//                        q[2][0], q[2][1], q[2][2], 0,
//                        0,0,0,1;

//    for (int i = 0; i<mesh->num_vertices(); ++i)
//    {
//        t.setZero();
//        sum = 0;

//        for (int j = 0; j< handles.size(); ++j)
//        {
//            t += transformation * weights.coeffRef(i, handles.at(j));
//            sum += weights.coeffRef(i, handles.at(j));
//        }

//        t += Eigen::Matrix4d::Identity() * (1-sum);

//        if(!t.isZero(0))
//        {
//            vertexVector =  Eigen::Vector4d (vertexCoords[i *3] - pos.x,
//                                             vertexCoords[i *3 +1] -pos.y,
//                                             vertexCoords[i *3 +2] -pos.z,
//                                             1);
//            vertexVector = t * vertexVector;
//            vertexVector = vertexVector * (1/vertexVector(3));

//            mesh->set_vertex(i, vec3<double>(vertexVector(0) + pos.x,
//                                             vertexVector(1) +pos.y,
//                                             vertexVector(2) +pos.z));
//        }
//    }

}

void LinearBlendSkinning::setIdHandle(int id)
{
    this->idHandle=id;
}
