#ifndef EUCLIDEANDISTANCEWEIGHTS_H
#define EUCLIDEANDISTANCEWEIGHTS_H

#include <QList>
#include "SkinningWeight.h"
#include "vec3.h"
#include "Eigen/Dense"
#include "Eigen/Sparse"

class EuclideanDistanceWeights : public SkinningWeight
{
public:
    EuclideanDistanceWeights();
    EuclideanDistanceWeights(Eigen::MatrixXd *skeleton, Eigen::MatrixXd *trimesh);
    EuclideanDistanceWeights(std::vector<Eigen::Triplet<double>> tripletList, int rows, int cols);

    void                        computeWeights();
    Eigen::SparseMatrix<double> getWeights();
    Eigen::MatrixXd             getMeshMatrix();

private:
    Eigen::MatrixXd             *meshMatrix;
    Eigen::MatrixXd             *handlesMatrix;
    Eigen::SparseMatrix<double> weights;

};

#endif // EUCLIDEANDISTANCEWEIGHTS_H
