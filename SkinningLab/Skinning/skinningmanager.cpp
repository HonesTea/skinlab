#include "skinningmanager.h"

SkinningManager::SkinningManager()
{

}

SkinningManager::SkinningManager(DrawableTrimesh* trimesh, Skel::CurveSkeleton* skeleton)
{
    mesh = trimesh;
    skel = skeleton;
    MatrixManipulation::coordsToMatrix(trimesh->vector_coords(), &meshMatrix);
    handleToMatrix(skeleton);
    init();
}

void SkinningManager::setTrimesh(DrawableTrimesh* trimesh)
{
    mesh = trimesh;
}

void SkinningManager::setHandleID(int _id)
{
    handleId = _id;
    dqs.sethandleId(_id);
}

void SkinningManager::init()
{
    wMode = WeightsMode::NOWEIGHTS;
    dMode = DeformationMode::LBS;
    biharmonicCalculated = false;
    boneHeatCalculated   = false;

    none = NoneWeights(handlesMatrix, meshMatrix);
    euclidean = EuclideanDistanceWeights(&handlesMatrix, &meshMatrix);

    using  ns = chrono::nanoseconds;
    using get_time = chrono::steady_clock ;
    auto start = get_time::now();

    euclidean.computeWeights();

    auto end = get_time::now();
    auto diff = end - start;
    cout<<"Elapsed time for Rigid weights is :  "<< chrono::duration_cast<ns>(diff).count()/1000000000.0<<" s "<<endl;

    lbs.setTrimesh(mesh);
    dqs.setTrimesh(mesh);

    dqs.setMeshMatrix(meshMatrix);
    dqs.setHandlesMatrix(handlesMatrix);
    dqs.setSkeleton(skel);
}

void SkinningManager::calculateWeights()
{
    none.computeWeights();
    euclidean.computeWeights();
    boneHeat.computeWeights();
    biharmonic.computeWeights();
}

void SkinningManager::applySkinning(Eigen::Matrix4d transformation, QList<int> handles)
{
    switch(dMode)
    {
        case LBS:
            switch(wMode)
            {
                case NOWEIGHTS:
                    lbs.applySkinning(transformation, handles, none.getWeights());
                break;

                case EUCLIDEAN:
                    lbs.applySkinning(transformation, handles, euclidean.getWeights());
                break;

                case BONEHEAT:
                    lbs.applySkinning(transformation, handles, boneHeat.getWeights());
                break;

                case BIHARMONIC:
                    lbs.applySkinning(transformation, handles, biharmonic.getWeights());
                break;
            }
        break;

        case DQS:
            switch(wMode)
            {
                case NOWEIGHTS:
                    dqs.applySkinning(transformation, handles, none.getWeights());
                break;

                case EUCLIDEAN:
                    dqs.applySkinning(transformation, handles, euclidean.getWeights());
                break;

                case BONEHEAT:
                    dqs.applySkinning(transformation, handles, boneHeat.getWeights());
                break;

                case BIHARMONIC:
                    dqs.applySkinning(transformation, handles, biharmonic.getWeights());
                break;
            }

        break;
    }
}

void SkinningManager::applySkinning(qglviewer::Quaternion rotation, qglviewer::Frame * const frame, QList<int> handles)
{
    switch (dMode)
    {
        case LBS:
            switch(wMode)
            {
                case NOWEIGHTS:
                    lbs.applySkinning(rotation, frame, handles, none.getWeights());
                break;

                case EUCLIDEAN:
                    lbs.applySkinning(rotation, frame, handles, euclidean.getWeights());
                break;

                case BONEHEAT:
                    lbs.applySkinning(rotation, frame, handles, boneHeat.getWeights());
                break;

                case BIHARMONIC:
                    lbs.applySkinning(rotation, frame, handles, biharmonic.getWeights());
                break;
            }

        break;

        case DQS:
            switch(wMode)
            {
                case NOWEIGHTS:
                    dqs.applySkinning(rotation, frame, handles, none.getWeights());
                break;

                case EUCLIDEAN:
                    dqs.applySkinning(rotation, frame, handles, euclidean.getWeights());
                break;

                case BONEHEAT:
                    dqs.applySkinning(rotation, frame, handles, boneHeat.getWeights());
                break;

                case BIHARMONIC:
                    dqs.applySkinning(rotation, frame, handles, biharmonic.getWeights());
                break;
            }
        break;
    }
}

void SkinningManager::setWeightMode(WeightsMode _mode)
{
    wMode = _mode;

    switch(wMode)
    {
        case WeightsMode::BONEHEAT:
        {
            if(!boneHeatCalculated)
            {
                using  ns = chrono::nanoseconds;
                using get_time = chrono::steady_clock ;
                auto start = get_time::now();

                boneHeat = BoneHeatWeights(&handlesMatrix, &meshMatrix);
                boneHeat.setDrawableTrimesh(mesh);
                boneHeat.computeWeights();
                boneHeatCalculated = true;

                auto end = get_time::now();
                auto diff = end - start;
                cout<<"Elapsed time for BoneHeat weights is :  "<< chrono::duration_cast<ns>(diff).count()/1000000000.0<<" s "<<endl;

            }
            break;
        }
        case WeightsMode::BIHARMONIC:
        {
            if(!biharmonicCalculated)
            {
                using  ns = chrono::nanoseconds;
                using get_time = chrono::steady_clock ;
                auto start = get_time::now();

                biharmonic = BoundedBiharmonicWeights(&handlesMatrix, &meshMatrix);
                biharmonic.setDrawableTrimesh(mesh);
                biharmonic.computeWeights();
                biharmonicCalculated = true;


                auto end = get_time::now();
                auto diff = end - start;
                cout<<"Elapsed time for Biharmonic weights is :  "<< chrono::duration_cast<ns>(diff).count()/1000000000.0<<" s "<<endl;

            }
            break;
        }
        default: break;
    }
}

void SkinningManager::setDeformMode(DeformationMode _mode)
{
    dMode = _mode;
}

Eigen::SparseMatrix<double> SkinningManager::getWeights(WeightsMode _mode)
{
    switch(_mode)
    {
        case NOWEIGHTS:
            return none.getWeights();
            break;
        case EUCLIDEAN:
            return euclidean.getWeights();
            break;
        case BONEHEAT:
            return boneHeat.getWeights();
            break;
        case BIHARMONIC:
            return biharmonic.getWeights();
            break;
        default: return Eigen::SparseMatrix<double> (meshMatrix.rows(), handlesMatrix.rows());
    }
}

void SkinningManager::saveWeights(const char* filename)
{
    ofstream fp;
    fp.open (filename);
    fp.precision(6);
    fp.setf( std::ios::fixed, std:: ios::floatfield ); // floatfield set to fixed

    if(!fp)
    {
        cerr << "ERROR : " << __FILE__ << ", line " << __LINE__ << " : save_weights() : couldn't open output file " << filename << endl;
        exit(-1);
    }

    fp << "row column value" << endl;

    Eigen::SparseMatrix<double> temp = euclidean.getWeights();
    fp << "rigid weights" << endl;
    fp << temp.nonZeros() << endl;

    for (int k=0; k< temp.outerSize(); ++k)
    {
        for (Eigen::SparseMatrix<double>::InnerIterator it(temp,k); it; ++it)
        {
            fp << it.row() << " " << it.col() << " " << it.value() << endl;
        }
    }

    if(!boneHeatCalculated)
    {
        boneHeat.computeWeights();
    }

    temp = boneHeat.getWeights();
    fp << "boneheat weights" << endl;
    fp << temp.nonZeros() << endl;

    for (int k=0; k< temp.outerSize(); ++k)
    {
        for (Eigen::SparseMatrix<double>::InnerIterator it(temp,k); it; ++it)
        {
            fp << it.row() << " " << it.col() << " " << it.value() << endl;
        }
    }

    if(!biharmonicCalculated)
    {
        biharmonic.computeWeights();
    }

    temp = biharmonic.getWeights();
    fp << "biharmonic weights" << endl;
    fp << temp.nonZeros() << endl;

    for (int k=0; k< temp.outerSize(); ++k)
    {
        for (Eigen::SparseMatrix<double>::InnerIterator it(temp,k); it; ++it)
        {
            fp << it.row() << " " << it.col() << " " << it.value() << endl;
        }
    }

}

void SkinningManager::loadWeights(const char* filename)
{
    ifstream file(filename);

    if (!file.is_open())
    {
        cerr << "ERROR : " << __FILE__ << ", line " << __LINE__ << " : load_OBJ() : couldn't open input file " << filename << endl;
        exit(-1);
    }

    int r, c;
    double v;
    string nonZeroes;
    int count;
    typedef Eigen::Triplet<double> T;
    std::vector<T> tripletList;
    tripletList.reserve(meshMatrix.rows() *4);

    string line;

    getline(file, line);//row column values
    getline(file, line);//rigid weights
    getline(file, line);

    istringstream iss(line);

    //rigid
    iss >> nonZeroes;
    count = atoi(nonZeroes.c_str());
    iss.clear();
    for(int i = 0; i < count; ++i)
    {
        getline(file, line);
        iss.str(line);
        iss >> r >> c >> v;
        tripletList.push_back(T(r, c, v));
        iss.clear();
    }
    euclidean = EuclideanDistanceWeights(tripletList, meshMatrix.rows(), handlesMatrix.rows());
    tripletList.clear();

    //Boneheat
    getline(file, line);
    getline(file, line);
    iss.str(line);
    iss >> nonZeroes;
    count = atoi(nonZeroes.c_str());
    iss.clear();
    for(int i = 0; i < count; ++i)
    {
        getline(file, line);
        iss.str(line);
        iss >> r >> c >> v;
        tripletList.push_back(T(r, c, v));
        iss.clear();
    }
    boneHeat = BoneHeatWeights(tripletList, meshMatrix.rows(), handlesMatrix.rows());
    tripletList.clear();

    //Biharmonics
    getline(file, line);
    getline(file, line);
    iss.str(line);
    iss >> nonZeroes;
    count = atoi(nonZeroes.c_str());
    iss.clear();
    for(int i = 0; i < count; ++i)
    {
        getline(file, line);
        iss >> r >> c >> v;
        iss.str(line);
        tripletList.push_back(T(r, c, v));
        iss.clear();
    }
    biharmonic = BoundedBiharmonicWeights(tripletList, meshMatrix.rows(), handlesMatrix.rows());
    tripletList.clear();

    boneHeatCalculated = true;
    biharmonicCalculated = true;

    file.close();
}

//can't use the MatrixManipulation static class because if the particular form of the skeleton
void SkinningManager::handleToMatrix(Skel::CurveSkeleton *skeleton)
{

    handlesMatrix.resize(skeleton->points.size(), 3);

    for(int i = 0; i < skeleton->points.size(); i++)
    {
        handlesMatrix(i,0) = skeleton->points[i].coord.x;
        handlesMatrix(i,1) = skeleton->points[i].coord.y;
        handlesMatrix(i,2) = skeleton->points[i].coord.z;
    }
}

