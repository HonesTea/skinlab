#include "noneweights.h"

NoneWeights::NoneWeights()
{

}

NoneWeights::NoneWeights(Eigen::MatrixXd skeleton, Eigen::MatrixXd trimesh)
{
   noneWeights = Eigen::SparseMatrix<double> (trimesh.rows(), skeleton.rows());
   noneWeights.setZero();
}

void NoneWeights::computeWeights()
{
    noneWeights.setZero();
}

Eigen::SparseMatrix<double> NoneWeights::getWeights()
{
    return noneWeights;
}

