#include "boundedbiharmonicweights.h"


BoundedBiharmonicWeights::BoundedBiharmonicWeights()
{

}

BoundedBiharmonicWeights::BoundedBiharmonicWeights(Eigen::MatrixXd *handles, Eigen::MatrixXd *mesh)
{
    handlesMatrix = handles;
    meshMatrix = mesh;
}

BoundedBiharmonicWeights::BoundedBiharmonicWeights(std::vector<Eigen::Triplet<double> > tripletList, int rows, int cols)
{
    weights = Eigen::SparseMatrix<double> (rows, cols);
    weights.setFromTriplets(tripletList.begin(), tripletList.end());
}

void BoundedBiharmonicWeights::setDrawableTrimesh(DrawableTrimesh *mesh)
{
    trimesh = mesh;
}

Eigen::SparseMatrix<double> BoundedBiharmonicWeights::getWeights()
{
    return weights;
}

void BoundedBiharmonicWeights::computeWeights()
{
    typedef Eigen::Triplet<double> T;
    vector<T> tripletList;
    tripletList.reserve(meshMatrix->rows() * handlesMatrix->rows());

    Eigen::SparseMatrix<double> laplacian;
    Eigen::SparseMatrix<double> massInv;

    computeVolumetricDomain();
    computeLaplacian(&laplacian);
    computeMass(&massInv);
    cout << "matrices done" << endl;

    assert(checkLaplacianValidity(laplacian.toDense()));
    assert(checkMassValidity(massInv, domain.getNumCoords()/3));

    Eigen::SparseMatrix<double> M (massInv.rows(), massInv.cols());

    for(int i = 0; i < M.rows(); ++i)
    {
        M.coeffRef(i,i) = sqrt( massInv.coeffRef(i,i) );
    }

    M = (( laplacian *massInv )) *laplacian;

    cout << "M done" << endl;

    computeBiharmonic(&tripletList, M);

    weights = Eigen::SparseMatrix<double> (meshMatrix->rows(), handlesMatrix->rows());
    weights.setZero();
    weights.setFromTriplets(tripletList.begin(), tripletList.end());
    MatrixManipulation::normalizeRows(weights);
    weights.makeCompressed();

    assert(checkWeightsValidity(weights));

    cout << "weights done" << endl;
}

void BoundedBiharmonicWeights::computeBiharmonic(vector<Eigen::Triplet<double>> *tripletList,
                                                 const Eigen::SparseMatrix<double>& M)
{
    GRBEnv env = GRBEnv();
    GRBModel model = GRBModel(env);
    model.getEnv().set(GRB_IntParam_OutputFlag, 0);
    model.getEnv().set(GRB_IntParam_NumericFocus, 0);

    try
    {
        vector<GRBVar>      xVars (M.rows());
        vector<GRBConstr>   handleConstrs(handlesMatrix->rows());

        for(int i = 0; i < M.rows(); ++i)
        {
            xVars[i] = model.addVar(0.0, 1.0, 0.0, GRB_CONTINUOUS, "");
        }
        model.update();

        //creating the objective function
        // minimize v
        GRBQuadExpr objective = 0;
        for (int k=0; k<M.outerSize(); ++k)
        {
            for (Eigen::SparseMatrix<double>::InnerIterator it(M,k); it; ++it)
            {
                objective += xVars[it.row()] * xVars[it.col()] *it.value() /2;
            }
        }
        model.setObjective(objective, GRB_MINIMIZE);
        model.update();
        cout << "objective updated" << endl;

        //Initializing handles constant value
        for(int i = 0; i < (int) handleConstrs.size(); ++i)
        {
            handleConstrs[i] = model.addConstr(xVars[i + meshMatrix->rows()] == 0);
        }
        model.update();



        for(int h = 0; h < handlesMatrix->rows(); ++h)
        {
            cout << "solving for handle " << h << endl;
            for(int j = 0; j < handlesMatrix->rows(); ++j)
            {
                if(j == h)
                {
                    handleConstrs[j].set(GRB_DoubleAttr_RHS, 1);
                }
                else
                {
                    handleConstrs[j].set(GRB_DoubleAttr_RHS, 0);
                }
            }
            model.update();

            model.optimize();

            for(int i = 0; i < meshMatrix->rows(); ++i)
            {
                tripletList->push_back(Eigen::Triplet<double>(i,h, xVars[i].get(GRB_DoubleAttr_X)));
            }
        }

    }
    catch (GRBException e)
    {
        cout << e.getErrorCode() << endl;
        cout << e.getMessage() << endl;

        cout << "computing IIS" << endl;
        try
        {

            model.computeIIS();
            cout << "\nThe following constraint(s) " << "cannot be satisfied:" << endl;
            GRBConstr* c = model.getConstrs();
            for (int i = 0; i < model.get(GRB_IntAttr_NumConstrs); ++i)
            {
              if (c[i].get(GRB_IntAttr_IISConstr) == 1)
              {
                cout << c[i].get(GRB_StringAttr_ConstrName) << endl;
              }
            }
        }
        catch (GRBException e)
        {
            cout << e.getErrorCode() << endl;
            cout << e.getMessage() << endl;
        }
    }
}

//not working
void BoundedBiharmonicWeights::computeConicProgram(vector<Eigen::Triplet<double>> *tripletList,
                                                 const Eigen::SparseMatrix<double>& M)
{
    GRBEnv env = GRBEnv();
    GRBModel model = GRBModel(env);
    model.getEnv().set(GRB_IntParam_OutputFlag, 0);
    model.getEnv().set(GRB_IntParam_NumericFocus, 0);
//    model.getEnv().set(GRB_IntParam_Threads, 7);

    try
    {
        //creating a t vector, same order as f, to convert the quadratic problem into a conic one
        vector<GRBVar>      fVars (M.rows());
        vector<GRBVar>      tVars (M.rows());
        vector<GRBLinExpr>  constrains (M.rows());
        vector<GRBConstr>   handleConstrs(handlesMatrix->rows());

        GRBVar              cVar;

        for(int i = 0; i < M.rows(); ++i)
        {
            fVars[i] = model.addVar(0.0, 1.0, 0.0, GRB_CONTINUOUS, "");
            tVars[i] = model.addVar(-GRB_INFINITY, GRB_INFINITY, 0.0, GRB_CONTINUOUS, "");
            constrains[i] = 0;
        }
        cVar = model.addVar(0, GRB_INFINITY, 0.0, GRB_CONTINUOUS, "");
        model.update();

        cout << "vars updated" << endl;

        //creating the objective function
        // minimize v
        GRBLinExpr objective = 0;
        for(int i = 0; i < (int)fVars.size(); ++i)
        {
            objective +=  fVars[i]*0 + tVars[i]*0;
        }

        model.setObjective(objective + cVar, GRB_MINIMIZE);
        model.update();
        cout << "objective updated" << endl;

        // adding linear constrains
        // Qx - t = 0
        for (int k=0; k<M.outerSize(); ++k)
        {
            for (Eigen::SparseMatrix<double>::InnerIterator it(M,k); it; ++it)
            {
                constrains[it.row()] += fVars[it.col()] *it.value();
            }
        }

        for(int i = 0; i < (int)fVars.size(); ++i)
        {
            constrains[i] += -tVars[i];
        }

        for(int i = 0; i < (int)fVars.size(); ++i)
        {
            model.addConstr(constrains[i] == 0);
        }
        model.update();

        //Adding quadratic constrain
        // 2v = summation(t[i]^2)
        GRBQuadExpr constrain = 0;
        for(int i = 0; i < (int) tVars.size(); ++i)
        {
               constrain = constrain + tVars[i] *tVars[i];
        }
        model.addQConstr( cVar *2 >= constrain);
        model.update();

        cout << "constraints updated" << endl;

        //Initializing handles constant value
        for(int i = 0; i < (int) handlesMatrix->rows(); ++i)
        {
            handleConstrs[i] = model.addConstr(fVars[i + meshMatrix->rows()] <= 0);
        }
        model.update();

        using  ns = chrono::nanoseconds;
        using get_time = chrono::steady_clock ;
        auto start = get_time::now();

        for(int h = 0; h < 3; ++h)
        {
            cout << "solving for handle " << h << endl;
            for(int j = 0; j < handlesMatrix->rows(); ++j)
            {
                if(j == h)
                {
                    handleConstrs[j].set(GRB_DoubleAttr_RHS, 10);
//                    fVars[j + meshMatrix.rows()].set(GRB_DoubleAttr_LB, 1);
//                    fVars[j + meshMatrix.rows()].set(GRB_DoubleAttr_UB, 1);
                }
                else
                {
                    handleConstrs[j].set(GRB_DoubleAttr_RHS, 0);
//                    fVars[j + meshMatrix.rows()].set(GRB_DoubleAttr_LB, 0);
//                    fVars[j + meshMatrix.rows()].set(GRB_DoubleAttr_UB, 0);
                }
            }
            model.update();

            model.optimize();

            for(int i = 0; i < meshMatrix->rows(); ++i)
            {
                tripletList->push_back(Eigen::Triplet<double>(i,h, fVars[i].get(GRB_DoubleAttr_X)));
            }
        }

        auto end = get_time::now();
        auto diff = end - start;
        cout<<"Elapsed time is :  "<< chrono::duration_cast<ns>(diff).count()/1000000000.0<<" s "<<endl;

    }
    catch (GRBException e)
    {
        cout << e.getErrorCode() << endl;
        cout << e.getMessage() << endl;

        cout << "computing IIS" << endl;
        try
        {

            model.computeIIS();
            cout << "\nThe following constraint(s) " << "cannot be satisfied:" << endl;
            GRBConstr* c = model.getConstrs();
            for (int i = 0; i < model.get(GRB_IntAttr_NumConstrs); ++i)
            {
              if (c[i].get(GRB_IntAttr_IISConstr) == 1)
              {
                cout << c[i].get(GRB_StringAttr_ConstrName) << endl;
              }
            }
        }
        catch (GRBException e)
        {
            cout << e.getErrorCode() << endl;
            cout << e.getMessage() << endl;
        }
    }

}



//Mesh is volumetric so the laplacian will be computed on dihedral angles.
void BoundedBiharmonicWeights::computeLaplacian(Eigen::SparseMatrix<double> *laplacian)
{
    Eigen::Vector3d v1;
    Eigen::Vector3d v2;
    Eigen::Vector3d v3;
    Eigen::Vector3d v4;

    Eigen::MatrixXd laplacianMatrix = Eigen::MatrixXd::Zero(domain.getNumCoords()/3, domain.getNumCoords()/3);

    for(int i = 0; i < domain.getNumTetrahedron(); ++i)
    {
        vector<int> tet = domain.getTetrahedron(i);

        v1 = Eigen::Vector3d(domain.getCoord(tet[0]*3  ),
                             domain.getCoord(tet[0]*3 +1),
                             domain.getCoord(tet[0]*3 +2));
        v2 = Eigen::Vector3d(domain.getCoord(tet[1]*3   ),
                             domain.getCoord(tet[1]*3 +1),
                             domain.getCoord(tet[1]*3 +2));
        v3 = Eigen::Vector3d(domain.getCoord(tet[2]*3   ),
                             domain.getCoord(tet[2]*3 +1),
                             domain.getCoord(tet[2]*3 +2));
        v4 = Eigen::Vector3d(domain.getCoord(tet[3]*3   ),
                             domain.getCoord(tet[3]*3 +1),
                             domain.getCoord(tet[3]*3 +2));

        double volume = abs(((v2-v1).dot((v3-v1).cross(v4-v1))))/6;

        //faces surfaces
        double sOAB, sABC, sOBC, sOCA;
        sOAB = ((v2 - v1).cross(v3 - v1)).norm()/2;
        sABC = ((v3 - v2).cross(v4 - v2)).norm()/2;
        sOBC = ((v3 - v1).cross(v4 - v1)).norm()/2;
        sOCA = ((v4 - v1).cross(v2 - v1)).norm()/2;

        //edges - math.stackexchange.com/questions/49330
        double eOA, eOB, eOC, eBC, eCA, eAB;
        double hSqr, jSqr, kSqr;
        eOA = (v2 - v1).norm();
        eOB = (v3 - v1).norm();
        eOC = (v4 - v1).norm();
        eBC = (v4 - v3).norm();
        eCA = (v2 - v4).norm();
        eAB = (v3 - v2).norm();

        hSqr = 1/16 *( 4*eOA*eOA*eBC*eBC - ((eOB*eOB + eCA*eCA) - (eOC*eOC + eAB*eAB))*((eOB*eOB + eCA*eCA) - (eOC*eOC + eAB*eAB)));
        jSqr = 1/16 *( 4*eOB*eOB*eCA*eCA - ((eOC*eOC + eAB*eAB) - (eOA*eOA + eBC*eBC))*((eOC*eOC + eAB*eAB) - (eOA*eOA + eBC*eBC)));
        kSqr = 1/16 *( 4*eOC*eOC*eAB*eAB - ((eOA*eOA + eBC*eBC) - (eOB*eOB + eCA*eCA))*((eOA*eOA + eBC*eBC) - (eOB*eOB + eCA*eCA)));

        //cosines of dihedral angle on edges
        double cosOA, cosOB, cosOC, cosBC, cosCA, cosAB;
        cosOA = (hSqr - sOCA*sOCA - sOAB*sOAB) / (-2 *sOCA *sOAB);
        cosOB = (jSqr - sOAB*sOAB - sOBC*sOBC) / (-2 *sOAB *sOBC);
        cosOC = (kSqr - sOBC*sOBC - sOCA*sOCA) / (-2 *sOBC *sOCA);
        cosBC = (hSqr - sABC*sABC - sOBC*sOBC) / (-2 *sABC *sOBC);
        cosCA = (jSqr - sABC*sABC - sOCA*sOCA) / (-2 *sABC *sOCA);
        cosAB = (kSqr - sABC*sABC - sOAB*sOAB) / (-2 *sABC *sOAB);

        //sines of dihedral angle on edges
        double sinOA, sinOB, sinOC, sinBC, sinCA, sinAB;
        sinOA = (3 *volume *eOA) / (2 *sOCA *sOAB);
        sinOB = (3 *volume *eOB) / (2 *sOAB *sOBC);
        sinOC = (3 *volume *eOC) / (2 *sOBC *sOCA);
        sinBC = (3 *volume *eBC) / (2 *sOBC *sABC);
        sinCA = (3 *volume *eCA) / (2 *sOCA *sABC);
        sinAB = (3 *volume *eAB) / (2 *sOAB *sABC);

        //cotangents of dihedral angle on edges
        double cotOA, cotOB, cotOC, cotBC, cotCA, cotAB;
        cotOA = eOA * cosOA / sinOA;
        cotOB = eOB * cosOB / sinOB;
        cotOC = eOC * cosOC / sinOC;
        cotBC = eBC * cosBC / sinBC;
        cotCA = eCA * cosCA / sinCA;
        cotAB = eAB * cosAB / sinAB;
        if (cotOA < EPSILON) cotOA = 0;
        if (cotOB < EPSILON) cotOB = 0;
        if (cotOC < EPSILON) cotOC = 0;
        if (cotBC < EPSILON) cotBC = 0;
        if (cotCA < EPSILON) cotCA = 0;
        if (cotAB < EPSILON) cotAB = 0;

        //filling laplacian
        laplacianMatrix(tet[2], tet[3]) += cotOA;
        laplacianMatrix(tet[3], tet[2]) += cotOA;
        laplacianMatrix(tet[1], tet[3]) += cotOB;
        laplacianMatrix(tet[3], tet[1]) += cotOB;
        laplacianMatrix(tet[1], tet[2]) += cotOC;
        laplacianMatrix(tet[2], tet[1]) += cotOC;
        laplacianMatrix(tet[0], tet[1]) += cotBC;
        laplacianMatrix(tet[1], tet[0]) += cotBC;
        laplacianMatrix(tet[0], tet[2]) += cotCA;
        laplacianMatrix(tet[2], tet[0]) += cotCA;
        laplacianMatrix(tet[0], tet[3]) += cotAB;
        laplacianMatrix(tet[3], tet[0]) += cotAB;

        laplacianMatrix(tet[0], tet[0]) -= (cotBC + cotCA + cotAB);
        laplacianMatrix(tet[1], tet[1]) -= (cotOB + cotOC + cotBC);
        laplacianMatrix(tet[2], tet[2]) -= (cotOA + cotOC + cotCA);
        laplacianMatrix(tet[3], tet[3]) -= (cotOA + cotOB + cotAB);

    }

//    for (int i = 0; i < laplacianMatrix.rows(); ++i)
//    {
//        laplacianMatrix(i,i) = -laplacianMatrix.row(i).sum();
//    }

    *laplacian = laplacianMatrix.sparseView();
}

void BoundedBiharmonicWeights::computeMass(Eigen::SparseMatrix<double> *massInv)
{
    Eigen::Vector3d v1;
    Eigen::Vector3d v2;
    Eigen::Vector3d v3;
    Eigen::Vector3d v4;

    Eigen::VectorXd massVector = Eigen::VectorXd::Zero(domain.getNumCoords()/3);

    for(int i = 0; i < domain.getNumTetrahedron(); ++i)
    {
        vector<int> tet = domain.getTetrahedron(i);

        v1 = Eigen::Vector3d(domain.getCoord(tet[0]*3  ),
                             domain.getCoord(tet[0]*3 +1),
                             domain.getCoord(tet[0]*3 +2));
        v2 = Eigen::Vector3d(domain.getCoord(tet[1]*3   ),
                             domain.getCoord(tet[1]*3 +1),
                             domain.getCoord(tet[1]*3 +2));
        v3 = Eigen::Vector3d(domain.getCoord(tet[2]*3   ),
                             domain.getCoord(tet[2]*3 +1),
                             domain.getCoord(tet[2]*3 +2));
        v4 = Eigen::Vector3d(domain.getCoord(tet[3]*3   ),
                             domain.getCoord(tet[3]*3 +1),
                             domain.getCoord(tet[3]*3 +2));

        double volume = abs(((v2-v1).dot((v3-v1).cross(v4-v1)))/6);

        massVector(tet[0]) += (volume/4);
        massVector(tet[1]) += (volume/4);
        massVector(tet[2]) += (volume/4);
        massVector(tet[3]) += (volume/4);
    }

    massInv->resize(domain.getNumCoords()/3, domain.getNumCoords()/3);
    for(int i = 0; i < domain.getNumCoords()/3; ++i)
    {
        massInv->coeffRef(i,i) = 1/(massVector(i));
    }
}

// compute the tetrahedral mesh, adding the skeleon discretized points inside the trimesh, using tetgen.
// the tetmesh will be loaded inside the volumetricDomain variable.
void BoundedBiharmonicWeights::computeVolumetricDomain()
{
    tetgenio           in, out, addin;
    tetgenio::facet   *f;
    tetgenio::polygon *p;

    // mesh vertices
    in.firstnumber    = 0;
    in.numberofpoints = trimesh->num_vertices();
    in.pointlist      = new REAL[in.numberofpoints *3];

    for(size_t i=0; i<trimesh->vector_coords().size(); ++i)
    {
        in.pointlist[i] = static_cast<REAL>(trimesh->vector_coords().at(i));
    }

    // skeleton vertices
    addin.firstnumber    = 0;
    addin.numberofpoints = handlesMatrix->rows();
    addin.pointlist      = new REAL[in.numberofpoints *3];
    for(size_t i = 0; i < (int) handlesMatrix->rows(); ++i)
    {
        addin.pointlist[i*3   ] = static_cast<REAL>((*handlesMatrix)(i,0));
        addin.pointlist[i*3 +1] = static_cast<REAL>((*handlesMatrix)(i,1));
        addin.pointlist[i*3 +2] = static_cast<REAL>((*handlesMatrix)(i,2));
    }

    // faces
    in.numberoffacets = trimesh->num_triangles();
    in.facetlist      = new tetgenio::facet[in.numberoffacets];

    for(int tid=0; tid<in.numberoffacets; ++tid)
    {
        f = &in.facetlist[tid];
        f->numberofpolygons = 1;
        f->polygonlist = new tetgenio::polygon[f->numberofpolygons];
        f->numberofholes = 0;
        f->holelist = NULL;
        p = &f->polygonlist[0];
        p->numberofvertices = 3;
        p->vertexlist = new int[p->numberofvertices];
        p->vertexlist[0] = trimesh->vector_triangles().at(tid * 3);
        p->vertexlist[1] = trimesh->vector_triangles().at(tid * 3 +1);
        p->vertexlist[2] = trimesh->vector_triangles().at(tid * 3 +2);
    }

    // edges
    vector< vector <int> > edges = trimesh->getVerticesAdjacency();
    in.numberofedges = trimesh->getNumEdges() *2;
    in.edgelist      = new int[in.numberofedges *2];
    int pos = 0;
    int k = 0;
    for(size_t i=0; i< edges.size(); ++i)
    {
        k += edges[i].size();

        for(size_t j=0; j<edges[i].size(); ++j)
        {
            in.edgelist[pos] = i;
            pos++;
            in.edgelist[pos] = edges[i][j];
            pos++;
        }
    }

    tetrahedralize("Yeiq1.4", &in, &out, &addin);

    // generate volumetric domain
    domain = TetMesh();
    domain.setNumCoords(out.numberofpoints *3);
    for(int vid=0; vid<out.numberofpoints; ++vid)
    {
        int vid_ptr = 3 * vid;
        domain.setVertex(vid,  static_cast<double>(out.pointlist[vid_ptr   ]),
                               static_cast<double>(out.pointlist[vid_ptr +1]),
                               static_cast<double>(out.pointlist[vid_ptr +2]));
    }

    domain.setNumTetrahedron(out.numberoftetrahedra);
    for(int tid=0; tid<out.numberoftetrahedra; ++tid)
    {
        int tid_ptr = 4 * tid;
        int v1 = out.tetrahedronlist[tid_ptr   ];
        int v2 = out.tetrahedronlist[tid_ptr +1];
        int v3 = out.tetrahedronlist[tid_ptr +2];
        int v4 = out.tetrahedronlist[tid_ptr +3];

        domain.setTetrahedron(tid, v1, v2, v3, v4);
    }

    domain.setNumEdges(out.numberofedges);
    for(int eid=0; eid<out.numberofedges; ++eid)
    {
        domain.addEdge(out.edgelist[eid*2], out.edgelist[eid*2 +1]);
    }
}

bool BoundedBiharmonicWeights::checkMassValidity(const Eigen::SparseMatrix<double>& M, const int& nValues)
{
    if(M.nonZeros() != nValues)
    {
        return false;
    }

    for (int k=0; k < M.outerSize(); ++k)
    {
      for (Eigen::SparseMatrix<double>::InnerIterator it(M,k); it; ++it)
      {
          if(it.row() != it.col())
          {
              cout << "Mass matrix: values outside the diagonal" << endl;
              return false;
          }

          if(it.value() <0)
          {
              cout << "Mass matrix: negative values" << endl;
              return false;
          }
      }
    }

    cout << "Mass matrix is valid" << endl;
    return true;
}

bool BoundedBiharmonicWeights::checkLaplacianValidity(const Eigen::MatrixXd& L)
{

    //check if the M matrix is definite positive
    Eigen::LLT<Eigen::MatrixXd> lltOfA(L); // compute the Cholesky decomposition of A
    if(lltOfA.info() == Eigen::NumericalIssue)
    {
        cout << ("Possibly non semi-positive definitie matrix!") << endl;
        return false;
    }

    if(L.cols() != L.rows())
    {
        cout << "not square matrix" << endl;
        return false;
    }

    for(int i = 0; i < L.rows(); ++i)
    {
        if( L.row(i).sum() > EPSILON || L.row(i).sum() < - EPSILON)
        {
            cout << "Laplacian matrix: sum on row diffrent from zero" << endl;
            return false;
        }

        for(int j = 0; j < L.cols(); ++j)
        {
            if(i == j)
            {
                if(L(i,j) >= 0)
                {
                    cout << "Laplacian matrix: value on diagonal not negative" << endl;
                    return false;
                }
            }
            else
            {
                if(L(i,j) != L(j,i))
                {
                    cout << "Laplacian matrix: not symmetryc" << endl;
                    return false;
                }
            }
        }
    }

    cout << "laplacian is valid" << endl;
    return true;
}

bool BoundedBiharmonicWeights::checkWeightsValidity(const Eigen::SparseMatrix<double> &W)
{
    for(int i = 0; i < W.rows(); ++i)
    {
        if(W.row(i).sum() > 1 + EPSILON || W.row(i).sum() < 1 - EPSILON)
        {
            cout << "Weight matrix: there is not partition of unity" << endl;
            return false;
        }
    }

    cout << "weights are valid" << endl;
    return true;
}
