#ifndef BOUNDEDBIHARMONICWEIGHTS_H
#define BOUNDEDBIHARMONICWEIGHTS_H

#define EPSILON 1e-5

#define NDEBUG


#include "tetgen/tetgen.h"
#include "Eigen/Sparse"
#include "Eigen/Dense"
//#include "eigen3/unsupported/Eigen/KroneckerProduct"

#include <chrono>
#include <iostream>
#include <assert.h>

#include "SkinningWeight.h"
#include "matrixmanipulation.h"
#include "tetmesh/tetmesh.h"

#include "gurobi_c++.h"

class BoundedBiharmonicWeights : public SkinningWeight
{
public:
    BoundedBiharmonicWeights();
    BoundedBiharmonicWeights(Eigen::MatrixXd *handles, Eigen::MatrixXd *mesh);
    BoundedBiharmonicWeights(std::vector<Eigen::Triplet<double>> tripletList, int rows, int cols);

    void computeWeights();
    void setDrawableTrimesh(DrawableTrimesh *mesh);
    Eigen::SparseMatrix<double> getWeights();


private:
    Eigen::MatrixXd             *meshMatrix;
    Eigen::MatrixXd             *handlesMatrix;
    TetMesh                     domain;
    Eigen::SparseMatrix<double> weights;
    vector<int>                 nearestHandle;
    DrawableTrimesh             *trimesh;

    void computeLaplacian(Eigen::SparseMatrix<double> *laplacian);
    void computeMass(Eigen::SparseMatrix<double> *massInv);
    void computeVolumetricDomain();

    void computeConicProgram (vector<Eigen::Triplet<double> > *tripletList,
                              const Eigen::SparseMatrix<double> &M);
    void computeBiharmonic   (vector<Eigen::Triplet<double>> *tripletList,
                              const Eigen::SparseMatrix<double>& M);

    bool checkMassValidity(const Eigen::SparseMatrix<double> & M, const int &nValues);
    bool checkLaplacianValidity(const Eigen::MatrixXd &L);
    bool checkWeightsValidity(const Eigen::SparseMatrix<double> & W);
};

#endif // BOUNDEDBIHARMONICWEIGHTS_H
