#ifndef BONEHEATWEIGHTS_H
#define BONEHEATWEIGHTS_H

#define EPSILON 1e-5

#include <chrono>
#include "SkinningWeight.h"
#include "Eigen/Dense"
#include "Eigen/Sparse"
#include "Eigen/SparseQR"
#include "Eigen/SparseLU"
#include "Eigen/SparseCholesky"
#include "Eigen/OrderingMethods"


class BoneHeatWeights : public SkinningWeight
{
public:
    BoneHeatWeights();
    BoneHeatWeights(Eigen::MatrixXd *handles, Eigen::MatrixXd *mesh);
    BoneHeatWeights(const std::vector<Eigen::Triplet<double>> tripletList, int rows, int cols);

    void computeWeights();
    void setDrawableTrimesh(DrawableTrimesh *mesh);
    Eigen::SparseMatrix<double> getWeights();

private:
    Eigen::MatrixXd             *meshMatrix;
    Eigen::MatrixXd             *handlesMatrix;
    DrawableTrimesh             *trimesh;
    Eigen::SparseMatrix<double> weights;


    void calculateEuclidean(Eigen::SparseMatrix<double> *euclidean);
    void calculateConfidence(Eigen::SparseMatrix<double> *confidence, Eigen::SparseMatrix<double> *euclidean);
    void calculateLaplacian(Eigen::SparseMatrix<double> *laplacian);
    void calculateMassMatrix(Eigen::SparseMatrix<double> *massInv);

    bool triangle_intersection(const Eigen::Vector3d   v1,
                               const Eigen::Vector3d   v2,
                               const Eigen::Vector3d   v3,
                               const Eigen::Vector3d   origin,
                               const Eigen::Vector3d   d);
};

#endif // BONEHEATWEIGHTS_H
