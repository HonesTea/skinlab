#include "dualquaternionskinning.h"

DualQuaternionSkinning::DualQuaternionSkinning()
{

}

DualQuaternionSkinning::DualQuaternionSkinning(DrawableTrimesh *m)
{
    mesh = m;
}

void DualQuaternionSkinning::applySkinning(Eigen::Matrix4d transformation, QList<int> handles, Eigen::SparseMatrix<double> weights)
{

    vector<double> vertexCoords = mesh->vector_coords();
    DualQuaternion q;
    DualQuaternion qt = DualQuaternion(1,0,0,0,0, transformation(0,3), transformation(1,3), transformation(2,3));
    double sum = 0;

    //working but slow
    for (int i = 0; i<mesh->num_vertices(); ++i)
    {
        q = DualQuaternion();
        sum = 0;

        for (int j = 0; j< handles.size(); ++j)
        {
            q = q + (qt * weights.coeffRef(i, handles.at(j)));
            sum += weights.coeffRef(i, handles.at(j));
        }

        q = q + (DualQuaternion::identity() * (1-sum));

        if(!q.isIdentity())
        {

            double vx = vertexCoords[i *3];
            double vy = vertexCoords[i *3 +1];
            double vz = vertexCoords[i *3 +2];

            q.normalize();
            DualQuaternion tmp = q;
            tmp.coniugateDual();
            tmp.coniugateQuat();

            DualQuaternion v = DualQuaternion(1,0,0,0,0,vx,vy,vz);

            v = q*v*tmp;

            mesh->set_vertex(i, vec3<double>(v.xe,v.ye,v.ze));

        }
    }
}

//still working on it
void DualQuaternionSkinning::applySkinning(qglviewer::Quaternion rotation, qglviewer::Frame * const frame, QList<int> handles, Eigen::SparseMatrix<double> weights)
{
    double sumWeights = 0;
    int previousRow = 0;
    Eigen::VectorXd sumWeightsVector (mesh->num_vertices());
    vector<double> vertexCoords = mesh->vector_coords();

    qglviewer::Vec worldAxis = frame->inverseTransformOf(rotation.axis());
    qglviewer::Vec pos = frame->position();
    float angle = rotation.angle();

    //working but slow
    for (int i = 0; i<mesh->num_vertices(); ++i)
    {
//        qglviewer::Quaternion qWorld(worldAxis , angle);

//        for (int j = 0; j< handles.size(); ++j)
//        {
//            int wij = weights.coeffRef(i, handles.at(j));
//            qWorld = qglviewer::Quaternion(qWorld[0] + qWorld[0]*wij,
//                                           qWorld[1] + qWorld[1]*wij,
//                                           qWorld[2] + qWorld[2]*wij,
//                                           qWorld[3] + qWorld[3]*wij);
//              sumWeights += wij;
//        }

//        qWorld = qWorld.normalized();
//        qglviewer::Vec point(vertexCoords[i*3], vertexCoords[i*3 +1], vertexCoords[i*3 +2]);
////        qglviewer::Vec pointRotation = qWorld.rotate(point - pos);
//        qWorld = qWorld * qglviewer::Quaternion(point[0] -pos.x,point[1] -pos.y,point[2] -pos.z,0) * qWorld.inverse();


//        if(sumWeights > 0)
//        {
////            mesh->set_vertex(i, vec3<double>(pos.x + pointRotation.x,
////                                             pos.y + pointRotation.y,
////                                             pos.z + pointRotation.z));

//            mesh->set_vertex(i, vec3<double>(pos.x + qWorld[0],
//                                             pos.y + qWorld[1],
//                                             pos.z + qWorld[2]));
//        }

//        sumWeights = 0;


        worldAxis = worldAxis / worldAxis.norm();

        float sin_a = sin( angle * 0.5f *2 );
        float cos_a = cos( angle * 0.5f *2 );
        double a    = cos_a;
        double x   = worldAxis.x * sin_a;
        double y   = worldAxis.y * sin_a;
        double z   = worldAxis.z * sin_a;
        double nor = sqrt(a*a + x*x + y*y + z*z);
        a = a/nor;
        x = x/nor;
        y = y/nor;
        z = z/nor;

        qglviewer::Quaternion qWorld(worldAxis, angle);
        qWorld = qWorld.normalized();
        DualQuaternion qt = DualQuaternion(a, x, y, z, 0, 0, 0, 0);
        DualQuaternion q = DualQuaternion::identity();
        sumWeights = 0;

        for (int j = 0; j< handles.size(); ++j)
        {
            q = q + (qt *weights.coeffRef(i, handles.at(j)));
            sumWeights += weights.coeffRef(i, handles.at(j));
        }

        q = q + (DualQuaternion::identity() * (1-sumWeights));


        double vx = vertexCoords[i *3] - pos.x;
        double vy = vertexCoords[i *3 +1] - pos.y;
        double vz = vertexCoords[i *3 +2] - pos.z;

        q.normalize();
//        DualQuaternion tmp = q;
//        tmp.coniugateQuat();
//        tmp.coniugateDual();


//        DualQuaternion v = DualQuaternion(1,0,0,0,0,vx,vy,vz);

//        v = q*v*tmp;

//        mesh->set_vertex(i, vec3<double>(v.xe + pos.x, v.ye + pos.y, v.ze + pos.z));


        ///this does lbs... somehow
        qglviewer::Vec v0 = qglviewer::Vec(q.x0, q.y0, q.y0);
        qglviewer::Vec ve = qglviewer::Vec(q.xe, q.ye, q.ye);

        qglviewer::Vec vn = qglviewer::Vec(v0[1]*ve[2]-v0[2]*ve[1], v0[2]*ve[0]-v0[0]*ve[2], v0[0]*ve[1]-v0[1]*ve[0]);

        qglviewer::Vec trans = (ve*q.w0 - v0*q.we + vn) * 2.f;

        // Rotate
        qWorld = qglviewer::Quaternion (q.x0, q.y0, q.z0, q.w0);
        qglviewer::Vec point(vertexCoords[i*3] - pos.x, vertexCoords[i*3 +1] -pos.y, vertexCoords[i*3 +2] -pos.z);
        qglviewer::Vec pointRotation = qWorld.rotate(point);
        pointRotation += trans;

        mesh->set_vertex(i, vec3<double>( pointRotation.x + pos.x,
                                          pointRotation.y + pos.y,
                                          pointRotation.z + pos.z));






//        qglviewer::Vec point(vertexCoords[i*3], vertexCoords[i*3 +1], vertexCoords[i*3 +2]);

//        qWorld = qglviewer::Quaternion (q.x0, q.y0, q.z0, q.w0);
//       qglviewer::Quaternion v1 = qWorld * qglviewer::Quaternion(point[0] -pos.x,point[1] -pos.y,point[2] -pos.z,0) *qWorld.inverse();

//       mesh->set_vertex(i, vec3<double>(v1[0] + pos.x,
//                                        v1[1] + pos.y,
//                                        v1[2] + pos.z));


//        qglviewer::Quaternion qWorld(worldAxis, angle*sumWeights);

//        qglviewer::Vec point(vertexCoords[i*3], vertexCoords[i*3 +1], vertexCoords[i*3 +2]);
//        qglviewer::Vec pointRotation = qWorld.rotate(point - pos);


//        if(sumWeights > 0)
//        {
////            mesh->set_vertex(i, vec3<double>(pos.x + pointRotation.x,
////                                             pos.y + pointRotation.y,
////                                             pos.z + pointRotation.z));
////            qWorld = qWorld.normalized();
//            qglviewer::Quaternion v1 = qWorld * qglviewer::Quaternion(point[0] -pos.x,point[1] -pos.y,point[2] -pos.z,0) *qWorld.inverse();

//            mesh->set_vertex(i, vec3<double>(v1[0] + pos.x,
//                                             v1[1] + pos.y,
//                                             v1[2] + pos.z));
//        }

//        sumWeights = 0;
    }

















//    const qglviewer::Vec worldAxis = frame->inverseTransformOf(rotation.axis());
//    qglviewer::Vec pos = frame->position();
//    const float angle = rotation.angle();
//    qglviewer::Quaternion qWorld(worldAxis, angle);
//    vector<DualQuaternion> transformationArray;


//    vector<double> vertexCoords = mesh->vector_coords();
//    DualQuaternion q;
//    double sum = 0;

//    Eigen::Vector3d v2;
//    Eigen::Vector3d pointT;
//    Eigen::Vector3d pivot;
//    Eigen::Vector3d e1;
//    Eigen::Vector3d e2;


//    pivot = handlesMatrix.row(handleId);


//    for (int i = 0; i < handles.size(); ++i)
//    {
//        v2 = handlesMatrix.row(i);

////        qglviewer::Vec point(v(0), v(1), v(2));
//        Skel::SkelPoint v = skel->points[handles[i]];
//        pointT = Eigen::Vector3d (v.coord.x,
//                                  v.coord.y,
//                                  v.coord.z);

//        if(handles(i) == i)
//        {

//        }

////        qglviewer::Vec pointRotation = qWorld.rotate(point - pos);

////        pointT = Eigen::Vector3d(pos.x + pointRotation.x,
////                                 pos.y + pointRotation.y,
////                                 pos.z + pointRotation.z);

//        e1 = v2 - pivot;
//        e2 = pointT - pivot;
//        if(e1.dot(e1)>0)
//            e1 = e1 / e1.norm();
//        if(e2.dot(e2)>0)
//            e2 = e2 / e2.norm();

//        double rotationAngle = acos(e1.dot(e2));
//        Eigen::Vector3d axis = e1.cross(e2);

//        transformationArray.push_back(DualQuaternion(e1.dot(e2) + 1, axis(0), axis(1), axis(2), 0, 0, 0, 0));
//    }

//    for (int i = 0; i < mesh->num_vertices(); ++i)
//    {
//        q = DualQuaternion();
//        sum = 0;

//        for (int j = 0; j< handles.size(); ++j)
//        {
//            q = q + (transformationArray[j] * weights.coeffRef(i, handles.at(j)));
//            sum += weights.coeffRef(i, handles.at(j));
//        }

//        q = q + (DualQuaternion::identity() * (1-sum));

//        if(!q.isIdentity())
//        {

////            double vx = vertexCoords[i *3];
////            double vy = vertexCoords[i *3 +1];
////            double vz = vertexCoords[i *3 +2];

//            Eigen::Vector3d vertex = meshMatrix.row(i);

//            q.normalize();
//            DualQuaternion tmp = q;
//            tmp.coniugateDual();
//            tmp.coniugateQuat();

//            DualQuaternion v = DualQuaternion(1,0,0,0,0,vertex(0),vertex(1),vertex(2));

//            v = q*v*tmp;

//            mesh->set_vertex(i, vec3<double>(v.xe,v.ye,v.ze));

//        }
//    }
}

void DualQuaternionSkinning::setTrimesh(DrawableTrimesh *m)
{
    mesh = m;
}

void DualQuaternionSkinning::setSkeleton(Skel::CurveSkeleton *_skel)
{
    skel = _skel;
}

void DualQuaternionSkinning::setHandlesMatrix(Eigen::MatrixXd _handles)
{
    handlesMatrix = _handles;
}

void DualQuaternionSkinning::setMeshMatrix(Eigen::MatrixXd _mesh)
{
    meshMatrix = _mesh;
}

void DualQuaternionSkinning::sethandleId(int _id)
{
    handleId = _id;
}

