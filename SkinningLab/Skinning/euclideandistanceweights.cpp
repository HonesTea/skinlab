#include "euclideandistanceweights.h"

EuclideanDistanceWeights::EuclideanDistanceWeights()
{

}

EuclideanDistanceWeights::EuclideanDistanceWeights(Eigen::MatrixXd *skeleton, Eigen::MatrixXd *trimesh)
{
    meshMatrix = trimesh;
    handlesMatrix = skeleton;
}

EuclideanDistanceWeights::EuclideanDistanceWeights(std::vector<Eigen::Triplet<double> > tripletList, int rows, int cols)
{
    weights = Eigen::SparseMatrix<double> (rows, cols);
    weights.setFromTriplets(tripletList.begin(), tripletList.end());
}

//the euclidean weights are:
//               {1 if d(v,hi) < d(v,hj) for any i!=j
// w i,j (v) =   {
//               {0 otherwise
void EuclideanDistanceWeights::computeWeights()
{
    double distance;
    double distance2;
    int nearest = 0;

    typedef Eigen::Triplet<double> T;
    std::vector<T> tripletList;
    cout << meshMatrix->rows() << endl;
    tripletList.reserve(meshMatrix->rows() *2);

    for(int i = 0; i < meshMatrix->rows(); ++i)
    {
        distance = (meshMatrix->row(i) - handlesMatrix->row(0)).norm();
        nearest = 0;

        for(int j = 0; j < handlesMatrix->rows(); ++j)
        {
            distance2 = (meshMatrix->row(i) - handlesMatrix->row(j)).norm();

            if(distance > distance2)
            {
                nearest = j;
                distance = distance2;
            }
        }
        tripletList.push_back(T(i, nearest, 1));
    }

    weights = Eigen::SparseMatrix<double> (meshMatrix->rows(),handlesMatrix->rows());
    weights.setFromTriplets(tripletList.begin(), tripletList.end());
}

Eigen::SparseMatrix<double> EuclideanDistanceWeights::getWeights()
{
    return weights;
}
