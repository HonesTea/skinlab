#ifndef NONEWEIGHTS_H
#define NONEWEIGHTS_H

#include "Skinning/SkinningWeight.h"
#include "Eigen/Sparse"
#include "Eigen/Dense"

class NoneWeights : public SkinningWeight
{
public:
    NoneWeights();
    NoneWeights(Eigen::MatrixXd skeleton, Eigen::MatrixXd trimesh);

    void computeWeights();

    Eigen::SparseMatrix<double> getWeights();

private:
    Eigen::SparseMatrix<double> noneWeights;
};

#endif // NONEWEIGHTS_H
