/**
 @author    Marco Livesu (marco.livesu@gmail.com)
 @copyright Marco Livesu 2014.
*/

#include "load_save_trimesh.h"

#include <iostream>
#include <fstream>
#include <sstream>
#include <stdlib.h>


using namespace std;


void load_OBJ(const char     * filename,
              vector<double> & xyz,
              vector<int>    & tri)
{
    ifstream file(filename);

    if (!file.is_open())
    {
        cerr << "ERROR : " << __FILE__ << ", line " << __LINE__ << " : load_OBJ() : couldn't open input file " << filename << endl;
        exit(-1);
    }

    string line;
    while (getline(file, line))
    {
        istringstream iss(line);

        string token;
        iss >> token;
        if (token.size() > 1) continue; // vn,fn  .... I don't care

        if (token[0] == 'v')
        {
            double x, y, z;
            iss >> x >> y >> z;
            xyz.push_back(x);
            xyz.push_back(y);
            xyz.push_back(z);
            //cout << "v " << x << " " << y << " " << z << endl;
        }
        else if (token[0] == 'f')
        {
            int v0, v1, v2;
            iss >> v0 >> v1 >> v2;
            tri.push_back(v0-1);
            tri.push_back(v1-1);
            tri.push_back(v2-1);
            //cout << "f " << v0 << " " << v1 << " " << v2 << endl;
        }
    }
    file.close();
}

void load_PLY (const char * filename,
               vector<double> & xyz,
               vector<int>    & tri)
{
    int vnum, fnum;

    ifstream file(filename);
    if (!file.is_open())
    {
        cerr << "ERROR : " << __FILE__ << ", line " << __LINE__ << " : load_OBJ() : couldn't open input file " << filename << endl;
        exit(-1);
    }

    string line;
    unsigned found;
    bool flag = false;

    //Retrieve informations from headers
    while(getline(file, line))
    {
        if (line.find("element vertex") != std::string::npos)
        {
            found = line.find_last_of(" ");
            vnum = atoi(line.substr(found+1).c_str());
        }
        if (line.find("element face") != std::string::npos)
        {
            found = line.find_last_of(" ");
            fnum = atoi(line.substr(found+1).c_str());
        }
        if (line.find("int flags") != std::string::npos)
        {
            flag = true;
        }
        if (line == "end_header")
            break;
    }

    //std::cout << vnum << " " << fnum << endl;

    double x,y,z;
//    double u,v,quality;
//    int flags;


    xyz.reserve(vnum*3);
    tri.reserve(fnum*3);

//    M.V = MatrixXd (vnum, 3);
//    M.F = MatrixXi (fnum, 3);
//    C.Vmesh = MatrixXd (vnum, 2);
//    C.QVmesh = VectorXi (vnum);

    //Get vertex coordinates for each vertex
    for(int i=0; i<vnum; i++)
    {
        getline(file, line);
        stringstream linestream(line);
        linestream >> x >> y >> z;
//        if(flag)
//            linestream >> x >> y >> z >> flags >> quality >>
//                                u >> v;
//        else linestream >> x >> y >> z >> quality >>
//                                u >> v;

        //Save coordinates for each vertex
        xyz.push_back(x);
        xyz.push_back(y);
        xyz.push_back(z);

//        M.V.row(i) = Vector3d(x,y,z);
//        C.Vmesh.row(i) = Vector2d(u,v);
//        C.QVmesh[i] = quality;

    }
    int tmp, v0,v1,v2;
    for(int i=0; i<fnum; i++)
    {
        getline(file, line);
        stringstream linestream(line);
        linestream >> tmp >> v0 >> v1 >> v2;
        tri.push_back(v0);
        tri.push_back(v1);
        tri.push_back(v2);
//        M.F.row(i) = Vector3i(v0,v1,v2);
    }

}

void load_OFF(const char *filename, std::vector<double> &xyz, std::vector<int> &tri)
{
    using namespace std;
    FILE * off_file = fopen(filename,"r");
    if(NULL==off_file)
    {
       printf("IOError: %s could not be opened...\n",filename);
       return;
     }
     tri.clear();
     xyz.clear();
     // First line is always OFF
     char header[1000];
     const std::string OFF("OFF");
     const std::string NOFF("NOFF");
     if(fscanf(off_file,"%s\n",header)!=1
         || !(
           string(header).compare(0, OFF.length(), OFF)==0 ||
           string(header).compare(0,NOFF.length(),NOFF)==0))
     {
       printf("Error: %s's first line should be OFF or NOFF not %s...",filename,header);
       fclose(off_file);
       return;
     }
     // Second line is #vertices #faces #edges
     int number_of_vertices;
     int number_of_faces;
     int number_of_edges;
     char tic_tac_toe;
     char line[1000];
     bool still_comments = true;
     while(still_comments)
     {
       fgets(line,1000,off_file);
       still_comments = line[0] == '#';
     }
     sscanf(line,"%d %d %d",&number_of_vertices,&number_of_faces,&number_of_edges);

     // Read vertices
     for(int i = 0;i<number_of_vertices; i++)
     {
       fgets(line, 1000, off_file);
       double x,y,z,nx,ny,nz;
       if(sscanf(line, "%lg %lg %lg %lg %lg %lg",&x,&y,&z,&nx,&ny,&nz)>= 3)
       {

         xyz.push_back(x);
         xyz.push_back(y);
         xyz.push_back(z);

       }else if(
           fscanf(off_file,"%[#]",&tic_tac_toe)==1)
       {
         char comment[1000];
         fscanf(off_file,"%[^\n]",comment);
       }else
       {
         printf("Error: bad line (%d) in %s\n",i,filename);
         if(feof(off_file))
         {
           fclose(off_file);
           return;
         }
       }
     }
     // Read faces
     for(int i = 0;i<number_of_faces; i++)
     {
         int v1,v2,v3, v4face;
         fscanf(off_file, "%d %d %d %d", &v4face, &v1, &v2, &v3);

         //Save current triangle face
         tri.push_back(v1);
         tri.push_back(v2);
         tri.push_back(v3);

     }
     fclose(off_file);
}

void save_OBJ(const char            * filename,
              std::vector<double>   & xyz,
              std::vector<int>      & tri,
              double                scaleFactor)
{


    ofstream fp;
    fp.open (filename);
    fp.precision(6);
    fp.setf( std::ios::fixed, std:: ios::floatfield ); // floatfield set to fixed

    if(!fp)
    {
        cerr << "ERROR : " << __FILE__ << ", line " << __LINE__ << " : save_OBJ() : couldn't open output file " << filename << endl;
        exit(-1);
    }

    //cout << "v " << xyz[3] << " " << xyz[4] << " " << xyz[5] << endl;

    for(int i=0; i<(int)xyz.size(); i+=3)
    {
        fp << "v " << xyz[i] /scaleFactor << " " << xyz[i+1] /scaleFactor << " " << xyz[i+2] /scaleFactor << endl;
    }

    for(int i=0; i<(int)tri.size(); i+=3)
    {
        fp << "f " << tri[i]+1 << " " << tri[i+1]+1 << " " << tri[i+2]+1 << endl;
    }

    fp.close();
}
