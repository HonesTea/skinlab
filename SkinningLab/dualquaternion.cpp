#include "dualquaternion.h"

DualQuaternion::DualQuaternion()
{
    w0 = 1;
    x0 = 0;
    y0 = 0;
    z0 = 0;

    we = 0;
    xe = 0;
    ye = 0;
    ze = 0;
}

DualQuaternion::DualQuaternion(Eigen::Quaterniond _q0, Eigen::Quaterniond _qe)
{
    _q0.normalize();
    w0 = _q0.w();
    x0 = _q0.x();
    y0 = _q0.y();
    z0 = _q0.z();

    we = _qe.w();
    xe = _qe.x();
    ye = _qe.y();
    ze = _qe.z();
}

DualQuaternion::DualQuaternion(double _w0, double _x0, double _y0, double _z0,
                               double _we, double _xe, double _ye, double _ze)
{
    w0 = _w0;
    x0 = _x0;
    y0 = _y0;
    z0 = _z0;

    we = _we;
    xe = _xe;
    ye = _ye;
    ze = _ze;
}

DualQuaternion DualQuaternion::identity()
{
    return DualQuaternion(1, 0, 0, 0, 0, 0, 0, 0);
}


void DualQuaternion::coniugateQuat()
{
    x0 = -x0;
    y0 = -y0;
    z0 = -z0;

    xe = -xe;
    ye = -ye;
    ze = -ze;
}

void DualQuaternion::coniugateDual()
{
    we = -we;
    xe = -xe;
    ye = -ye;
    ze = -ze;
}

void DualQuaternion::normalize()
{
    double _norm = sqrt(w0*w0 + x0*x0 + y0*y0 + z0*z0);

    w0 = w0 / _norm;
    x0 = x0 / _norm;
    y0 = y0 / _norm;
    z0 = z0 / _norm;

    we = we / _norm;
    xe = xe / _norm;
    ye = ye / _norm;
    ze = ze / _norm;
}

double DualQuaternion::norm()
{
    DualQuaternion p = DualQuaternion(w0, x0, y0, z0, -we, -xe, -ye, -ze);
    p = *this*p;

    double _norm = sqrt(p.w0*p.w0 + p.x0*p.x0 + p.y0*p.y0 + p.z0*p.z0);

    return _norm + ((p.w0*p.we + p.x0*p.xe + p.y0*p.ye + p.z0*p.ze) / _norm);
}

bool DualQuaternion::isIdentity()
{
    return ((w0==1) && ((x0 == y0) && (y0 == z0) && (we == xe) && (ye == xe) && (we == ye) && (ye == ze) && (ze == z0) && (ze == 0)));
}

Eigen::Quaterniond DualQuaternion::getReal()
{
    return Eigen::Quaterniond(w0, x0, y0, z0);
}

Eigen::Quaterniond DualQuaternion::getDual()
{
    return Eigen::Quaterniond(we, xe, ye, ze);
}

DualQuaternion DualQuaternion::operator+ (DualQuaternion _q)
{

    return DualQuaternion (w0+_q.w0, x0+_q.x0, y0+_q.y0, z0+_q.z0,
                           we+_q.we, xe+_q.xe, ye+_q.ye, ze+_q.ze);
}

DualQuaternion DualQuaternion::operator* (DualQuaternion _q)
{
    return DualQuaternion (w0*_q.w0 - x0*_q.x0 - y0*_q.y0 - z0*_q.z0,
                           w0*_q.x0 + x0*_q.w0 + y0*_q.z0 - z0*_q.x0,
                           w0*_q.y0 + y0*_q.w0 + z0*_q.x0 - x0*_q.z0,
                           w0*_q.z0 + z0*_q.w0 + x0*_q.y0 - y0*_q.x0,
                           w0*_q.we - x0*_q.xe - y0*_q.ye - z0*_q.ze + we*_q.w0 - xe*_q.x0 - ye*_q.y0 - ze*_q.z0,
                           w0*_q.xe + x0*_q.we + y0*_q.ze - z0*_q.xe + we*_q.x0 + xe*_q.w0 + ye*_q.z0 - ze*_q.x0,
                           w0*_q.ye + y0*_q.we + z0*_q.xe - x0*_q.ze + we*_q.y0 + ye*_q.w0 + ze*_q.x0 - xe*_q.z0,
                           w0*_q.ze + z0*_q.we + x0*_q.ye - y0*_q.xe + we*_q.z0 + ze*_q.w0 + xe*_q.y0 - ye*_q.x0);

//    return DualQuaternion (w0*_q.w0 - x0*_q.x0 - y0*_q.y0 - z0*_q.z0,
//                           w0*_q.x0 + x0*_q.w0 + y0*_q.z0 - z0*_q.x0,
//                           w0*_q.y0 + y0*_q.w0 + z0*_q.x0 - x0*_q.z0,
//                           w0*_q.z0 + z0*_q.w0 + x0*_q.y0 - y0*_q.x0,
//                           0, 0, 0, 0);
}


DualQuaternion DualQuaternion::operator* (double k)
{
    return DualQuaternion (w0*k, x0*k, y0*k, z0*k,
                           we*k, xe*k, ye*k, ze*k);
}

