#include "deformationframeconstraint.h"

DeformationFrameConstraint::DeformationFrameConstraint()
{
    isAxisContraintEnabled = false;
    setDirectionConstraint(NONE_CONSTR_AXIS);
}

void DeformationFrameConstraint::clearSet()
{
    objects_.clear();
}

void DeformationFrameConstraint::setSkel(Skel::CurveSkeleton *s)
{
    skel = s;
    skinner = SkinningManager(mesh, skel);
}

void DeformationFrameConstraint::setTrimesh(DrawableTrimesh *m)
{
    this->mesh = m;
}

void DeformationFrameConstraint::setDirectionConstraint(Axis axisId)
{   
    if(axisId==NONE_CONSTR_AXIS){
        isAxisContraintEnabled = false;
        constrainedAxis = Vec(1.0,1.0,1.0);
        setTranslationConstraintType(AxisPlaneConstraint::FREE);
        setRotationConstraintType(AxisPlaneConstraint::FREE);
    } else {
        //Set direction according to axis
        isAxisContraintEnabled = true;
        constrainedAxis = Vec(0.0,0.0,0.0);
        constrainedAxis[axisId] = 1.0;
        setTranslationConstraintType(AxisPlaneConstraint::AXIS);
        setRotationConstraintType(AxisPlaneConstraint::AXIS);
    }
    setTranslationConstraintDirection(constrainedAxis);
    setRotationConstraintDirection(constrainedAxis);
    //cout << "AXIS" << axisId;
    //cout << "VEC:" << constrainedAxis[0] << constrainedAxis[1] << constrainedAxis[2] << endl;;
}

void DeformationFrameConstraint::setWeightMode(WeightsMode _mode)
{
    skinner.setWeightMode(_mode);
}

void DeformationFrameConstraint::setDeformMode(DeformationMode _mode)
{
    skinner.setDeformMode(_mode);
}

Eigen::SparseMatrix<double> DeformationFrameConstraint::getWeights(WeightsMode _mode)
{
    return skinner.getWeights(_mode);
}

void DeformationFrameConstraint::saveWeights(const char *filename)
{
    skinner.saveWeights(filename);
}

void DeformationFrameConstraint::loadWeights(const char *filename)
{
    skinner.loadWeights(filename);
}

void DeformationFrameConstraint::addObjectToSet(int vId)
{
    objects_.append(vId);
}

void DeformationFrameConstraint::setHandleID(int id)
{
    handleId = id;
    skinner.setHandleID(id);
}

void DeformationFrameConstraint::constrainTranslation(qglviewer::Vec &translation, Frame *const frame)
{

    Eigen::Matrix4d transformMatrix = getHomogeneousTranslationMatrix(translation.x, translation.y, translation.z);
    skinner.applySkinning(transformMatrix, objects_);

    if(isAxisContraintEnabled){
        Vec proj = frame->rotation().rotate(translationConstraintDirection());
        translation.projectOnAxis(proj);
    }

    for (int i = 0; i < objects_.size(); i++)
    {
        //cout << "VEC:" << constrainedAxis[0] << constrainedAxis[1] << constrainedAxis[2] << endl;;
        Skel::SkelPoint v = skel->points[objects_[i]];

        double tempX = v.coord.x + translation.x;
        double tempY = v.coord.y + translation.y;
        double tempZ = v.coord.z + translation.z;

        skel->setNode(objects_[i], tempX, tempY, tempZ);
    }
}

void DeformationFrameConstraint::constrainRotation(qglviewer::Quaternion &rotation, Frame *const frame)
{
    skinner.applySkinning(rotation, frame, objects_);

    // A little bit of math. Easy to understand, hard to guess (tm).
    // rotation is expressed in the frame local coordinates system. Convert it back to world coordinates.

//    if(isAxisContraintEnabled){
//        Vec axis = rotationConstraintDirection();
//        Vec quat = Vec(rotation[0], rotation[1], rotation[2]);
//        quat.projectOnAxis(axis);
//        rotation = Quaternion(quat, 2.0*acos(rotation[3]));
//    }

    const Vec worldAxis = frame->inverseTransformOf(rotation.axis());
    Vec pos = frame->position();
    const float angle = rotation.angle();
    Quaternion qWorld(worldAxis, angle);

    for (int i = 0; i < objects_.size(); i++)
    {
        //cout << "VEC:" << constrainedAxis[0] << constrainedAxis[1] << constrainedAxis[2] << endl;;
        //Quaternion qObject(rotation.axis(), angle);

        Skel::SkelPoint v = skel->points[objects_[i]];

        Vec point(v.coord.x, v.coord.y, v.coord.z);

        /*if(isAxisContraintEnabled)
        {
            Vec quat = Vec(qWorld[0], qWorld[1], qWorld[2]);
            quat.projectOnAxis(constrainedAxis);
            qWorld = Quaternion(quat, 2.0*acos(rotation[3]));
        }*/

        Vec pointRotation = qWorld.rotate(point - pos);

        double tempX = pos.x + pointRotation.x;
        double tempY = pos.y + pointRotation.y;
        double tempZ = pos.z + pointRotation.z;

        skel->setNode(objects_[i], tempX, tempY, tempZ);
    }
}

Eigen::Matrix4d DeformationFrameConstraint::getHomogeneousTranslationMatrix(double x, double y, double z)
{
    Eigen::Matrix4d out;

    for (int i = 0; i<4; ++i)
    {
        for (int j = 0; j<4; ++j)
        {
            if(i == j)
            {
                out(i,j) = 1;
            }
            else
            {
                out(i,j) = 0;
            }
        }
    }

    out(0,3) = x;
    out(1,3) = y;
    out(2,3) = z;

    return out;
}

Eigen::Matrix4d DeformationFrameConstraint::getHomogeneousRotationMatrixFromQuaternion(Quaternion q)
{
    GLdouble r[4][4];
    q.getMatrix(r);
    Eigen::Matrix4d out;

    for (int i = 0; i<4; i++)
    {
        for (int j = 0; j<4; j++)
        {
            out(i,j) = (double) r[i][j];
        }
    }

    return out;

}
