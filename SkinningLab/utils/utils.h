#ifndef UTILS_H
#define UTILS_H

#include "vec3.h"
#ifdef __APPLE__
#include <gl.h>
#include <glu.h>
#else
#include <GL/gl.h>
#include <GL/glu.h>
#endif

using namespace std;

typedef pair<int,int> edge;

// Different selection modes
enum OperationMode {NONE, ADD, REMOVE, DEFORM };

//Different weights modes
enum WeightsMode {NOWEIGHTS, EUCLIDEAN, BONEHEAT, BIHARMONIC};

//Different deformation modes
enum DeformationMode {LBS, DQS};

//Different Cage generation interaction modes
enum CagenOperationMode { CAMERA_CAGEN, SPLIT_CAGEN };

//Different coloration modes
enum ColorationMode { VERTEX_COLOR, TRIANGLE_COLOR, INFLUENCE_COLOR };

//Different barycentric coordinates modes
enum BarycentricMode { NONE_COORDS, GREEN_COORDS, MVC_COORDS };

//Axis selection
enum Axis { X_CONSTR_AXIS, Y_CONSTR_AXIS, Z_CONSTR_AXIS, NONE_CONSTR_AXIS};

inline void gl_sphere(float x, float y, float z, float radius, bool selected)
{
        glEnable(GL_LIGHTING);
        glShadeModel(GL_SMOOTH);
        if(selected)
        {
            glColor3d(0.9, 0.2, 0.2);
        } else {
            glColor3d(0.2, 0.2, 0.9);
        }
        glPushMatrix();
        glTranslated(x, y, z);
        GLUquadric *sphere = gluNewQuadric();
        gluQuadricOrientation(sphere,GLU_OUTSIDE);
        gluSphere(sphere, radius*0.6, 30, 30);
        glPopMatrix();
}

// Used to order a pair of vertices based on their index (v1,id1) (v2,id2)
inline bool pairCompare(const std::pair<vec3d, int>& firstElem, const std::pair<vec3d, int>& secondElem) {
  return firstElem.second < secondElem.second;

}

#endif // UTILS_H
