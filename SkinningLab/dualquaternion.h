#ifndef DUALQUATERNION_H
#define DUALQUATERNION_H

#include <math.h>
#include "Eigen/Geometry"

class DualQuaternion
{
public:
    DualQuaternion();
    DualQuaternion(Eigen::Quaterniond _q0, Eigen::Quaterniond _qe);
    DualQuaternion (Eigen::Vector3d v);
    DualQuaternion(double _w0, double _x0, double _y0, double _z0,
                   double _we, double _xe, double _ye, double _ze);

    static DualQuaternion identity();

    void coniugateQuat();
    void coniugateDual();
    void normalize();
    double norm();
    bool isIdentity();
    Eigen::Quaterniond getReal();
    Eigen::Quaterniond getDual();

    DualQuaternion operator+ (DualQuaternion _q);
    DualQuaternion operator* (DualQuaternion _q);
    DualQuaternion operator* (double w);

    double w0;
    double x0;
    double y0;
    double z0;
    double we;
    double xe;
    double ye;
    double ze;

private:

};

#endif // DUALQUATERNION_H
