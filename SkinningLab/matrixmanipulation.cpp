#include "matrixmanipulation.h"

MatrixManipulation::MatrixManipulation()
{

}

void MatrixManipulation::removeRows(Eigen::MatrixXd *matrix, int rowsStart, int rowsToRemove)
{
    unsigned int numRows = matrix->rows() - rowsToRemove;
    unsigned int numCols = matrix->cols();

    Eigen::MatrixXd temp (numRows, numCols);
    temp.block(0, 0, rowsStart, numCols) = matrix->block(0, 0, rowsStart, numCols);
    temp.block(rowsStart, 0, matrix->rows() -rowsStart - rowsToRemove , numCols) =
               matrix->block(rowsStart + rowsToRemove, 0, matrix->rows() -rowsStart - rowsToRemove, numCols);
    *matrix = temp;
}

void MatrixManipulation::removeColumns(Eigen::MatrixXd *matrix, int colsStart, int colsToRemove)
{
    unsigned int numRows = matrix->rows();
    unsigned int numCols = matrix->cols() - colsToRemove;

    Eigen::MatrixXd temp (numRows, numCols);
    temp.block(0, 0, numRows, colsStart) = matrix->block(0, 0, numRows, colsStart);
    temp.block(0, colsStart, numRows, matrix->cols() - colsStart - colsToRemove) =
               matrix->block(0, colsStart + colsToRemove, numRows, matrix->cols() - colsStart - colsToRemove);
    *matrix = temp;
}

void MatrixManipulation::addRows(Eigen::MatrixXd *matrix, int rowsStart, int rowsNumber)
{
    //update matrix size
    int oldNumberOfRows = matrix->rows();
    matrix->conservativeResize(oldNumberOfRows + rowsNumber, matrix->cols());

    //translate values to their proper new position
    matrix->block(rowsStart + rowsNumber, 0, oldNumberOfRows - rowsStart, matrix->cols()) =
            matrix->block(rowsStart, 0, oldNumberOfRows - rowsStart, matrix->cols());

    //fill the new lines with zeroes
    matrix->block(rowsStart, 0, rowsNumber, matrix->cols()) =
            Eigen::MatrixXd::Zero(rowsNumber, matrix->cols());
}

//will apply partition of unity to the weights, i.e. sum on the row equal to one.
//this can be done in post processing cause it doesnt change the results, somehow.
void MatrixManipulation::normalizeRows(Eigen::SparseMatrix<double>& M)
{
    for(int i = 0; i< M.rows(); ++i)
    {
        M.row(i) /= M.row(i).sum();
    }
}

//https://en.wikipedia.org/wiki/Kronecker_product
//function optimized to calculate the product between a matrix M and an identity matrix I.
Eigen::SparseMatrix<double> MatrixManipulation::kroneckerProductIxM(const Eigen::SparseMatrix<double>& M, int sizeI)
{

    typedef Eigen::Triplet<double> T;
    std::vector<T> tripletList;
    tripletList.reserve(M.rows()*sizeI);

    Eigen::SparseMatrix<double> kroneckerMatrix (M.rows()*sizeI, M.rows()*sizeI);

    for (int k = 0; k<M.outerSize(); ++k)
    {
        for (Eigen::SparseMatrix<double>::InnerIterator it(M,k); it; ++it)
        {
            for(int i = 0; i < sizeI; ++i)
            {
                tripletList.push_back(T(it.row() + M.rows()*i, it.col() + M.cols()*i, it.value()));
            }
        }
    }

    kroneckerMatrix.setFromTriplets(tripletList.begin(), tripletList.end());
    kroneckerMatrix.makeCompressed();

    return kroneckerMatrix;
}

void MatrixManipulation::coordsToMatrix(const std::vector<double>& coords, Eigen::MatrixXd* M)
{
    M->resize(coords.size()/3, 3);

    for (int i = 0; i < coords.size()/3; i++)
    {
        (*M)(i,0) = coords[i*3];
        (*M)(i,1) = coords[i*3 +1];
        (*M)(i,2) = coords[i*3 +2];
    }
}

