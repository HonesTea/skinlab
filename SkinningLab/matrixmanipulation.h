#ifndef MATRIXMANIPULATION_H
#define MATRIXMANIPULATION_H

#include "Eigen/Sparse"

class MatrixManipulation
{
public:
    MatrixManipulation();

    static void coordsToMatrix(const std::vector<double>& coords, Eigen::MatrixXd* M);

    static void normalizeRows(Eigen::SparseMatrix<double>& M);

    static void addRows(Eigen::MatrixXd *matrix, int rowsStart, int rowsNumber);
    static void removeRows(Eigen::MatrixXd *matrix, int rowsStart, int rowsToRemove);
    static void removeColumns(Eigen::MatrixXd *matrix, int colsStart, int colsToRemove);

    static Eigen::SparseMatrix<double> kroneckerProductIxM(const Eigen::SparseMatrix<double> &M, int sizeI);
};

#endif // MATRIXMANIPULATION_H
