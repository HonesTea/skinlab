#include "save_manager.h"
#include "ui_save_manager.h"

Save_Manager::Save_Manager(QWidget *parent) :
    QDockWidget(parent),
    ui(new Ui::Save_Manager)
{
    ui->setupUi(this);
}

Save_Manager::~Save_Manager()
{
    delete ui;
}

void Save_Manager::on_saveMeshButton_clicked()
{
    emit saveMesh();
}

void Save_Manager::on_saveSkelButton_clicked()
{
    emit saveSkel();
}

void Save_Manager::on_SaveWeights_clicked()
{
    emit saveWeights();
}
