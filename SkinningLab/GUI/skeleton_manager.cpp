#include "skeleton_manager.h"
#include "ui_skeletonmanager.h"

SkeletonManager::SkeletonManager(QWidget *parent) : QDockWidget(parent), ui(new Ui::SkeletonManager)
{
    ui->setupUi(this);
}

SkeletonManager::~SkeletonManager()
{
    delete ui;
}

void SkeletonManager::on_LoadSkeleton_clicked()
{
//    ui->DeformationGroupBox->setEnabled(true);
    ui->weightsGroupBox->setEnabled(true);
    emit loadSkeleton();
}

void SkeletonManager::on_EuclideaDistance_clicked()
{
    emit activateEuclideanWeights();
}

void SkeletonManager::on_BoneHeat_clicked()
{
    emit activateBoneHeatWeights();
}

void SkeletonManager::on_LBS_clicked()
{
    emit activateLinearBlendSkinning();
}

void SkeletonManager::on_DQS_clicked()
{
    emit activateDualQuaternionSkinning();
}

void SkeletonManager::on_Biharmonic_clicked()
{
    emit activateBiharmonicWeights();
}

void SkeletonManager::on_visualizeWeightsBox_clicked(bool checked)
{
    emit weightsVisualization(checked);
}

void SkeletonManager::on_None_clicked()
{
    emit activateNoneWeights();
}

void SkeletonManager::on_LoadButton_clicked()
{
    emit loadWeights();
}
