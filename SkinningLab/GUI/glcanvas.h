/**
 @author    Marco Livesu (marco.livesu@gmail.com)
 @copyright Marco Livesu 2014.
*/

#ifndef GLCANVAS_H
#define GLCANVAS_H

#include <QGLViewer/qglviewer.h>
#include <QGLViewer/manipulatedFrame.h>
#include <QGLViewer/manipulatedCameraFrame.h>
#include <QGLWidget>
#include <QMouseEvent>
#include <vector>

#ifdef __APPLE__
#include <gl.h>
#else
#include <GL/gl.h>
#endif

#include "trimesh/drawable_trimesh.h"
#include "deformationframeconstraint.h"
#include "bbox.h"
#include "drawable_object.h"
#include "skel/import.h"
#include "skel/paint.h"
#include "skel/draw.h"
#include "skel/skelpoint/base.h"
#include "utils/utils.h"

using namespace std;

class GLcanvas : public QGLViewer
{
    public:

        GLcanvas(QWidget * parent = NULL);

        void init();
        void draw();
        void clear();
        void fit_scene();
        void set_clear_color(const QColor & color);

        void mousePressEvent    (QMouseEvent* e);
        void mouseMoveEvent     (QMouseEvent *e);
        void mouseReleaseEvent  (QMouseEvent *e);
        void endSelection       (const QPoint&);

        void setOperationMode   (OperationMode _mode);
        void setWeightsMode     (WeightsMode _mode);
        void setDeformationMode (DeformationMode _mode);

        int  push_obj           (const DrawableObject * obj);
        int  set_skeleton       (const Skel::CurveSkeleton* s);
        void set_trimesh        (const DrawableTrimesh* trimesh);
        void saveWeights        (const char* filename);
        void loadWeights        (const char *filename);
        void computeInfluence   ();
        void setWVisualization  (bool b);

    private:

        void startManipulation          ();
        void addIdToSelection           (int id);
        void removeIdFromSelection      (int id);
        void drawSelectionRectangle     () const;
        void drawWithNames              ();
        void setDeformationFramePosition();
        int  findPivot();

        QColor                          clear_color;
        vector<const DrawableObject *>  drawlist;
        DrawableTrimesh*                mesh;
        Skel::CurveSkeleton*            skeleton;
        QList<int>                      selection_;
        DeformationFrameConstraint *    deformationFrameConstraint;
        OperationMode                   modeO;
        WeightsMode                     modeW;
        QRect                           rectangle_;

        Axis currentConstrainedAxis;
        bool isRectangleActive;
        bool isSkelLoaded;
        bool weightsVisualization;
        bool biharmonicCalculated;
        bool boneHeatCalculated;
};

#endif // GLCANVAS_H
