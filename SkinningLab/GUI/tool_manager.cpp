#include "tool_manager.h"
#include "ui_tool_manager.h"

Tool_manager::Tool_manager(QWidget *parent) : QDockWidget(parent), ui(new Ui::Tool_manager)
{
    ui->setupUi(this);
}

Tool_manager::~Tool_manager()
{
    delete ui;
}

void Tool_manager::on_deselectButton_clicked()
{
    activate_deselection();
}

void Tool_manager::on_deformButton_clicked()
{
    activate_deform();
}

void Tool_manager::on_selectButton_clicked()
{
    activate_selection();
}

void Tool_manager::on_cameraButton_clicked()
{
    activate_camera();
}

void Tool_manager::activate_camera()
{
    emit activate_camera_mode();
}

void Tool_manager::activate_selection()
{
    emit activate_selection_mode();
}

void Tool_manager::activate_deselection()
{
    emit activate_deselection_mode();
}

void Tool_manager::activate_deform()
{
    emit activate_deform_mode();
}
