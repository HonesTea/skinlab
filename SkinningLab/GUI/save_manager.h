#ifndef SAVE_MANAGER_H
#define SAVE_MANAGER_H

#include <QDockWidget>

namespace Ui {
class Save_Manager;
}

class Save_Manager : public QDockWidget
{
    Q_OBJECT

public:
    explicit Save_Manager(QWidget *parent = 0);
    ~Save_Manager();

private slots:
    void on_saveMeshButton_clicked();

    void on_saveSkelButton_clicked();

    void on_SaveWeights_clicked();

signals:
    void saveMesh();
    void saveSkel();
    void saveWeights();

private:
    Ui::Save_Manager *ui;
};

#endif // SAVE_MANAGER_H
