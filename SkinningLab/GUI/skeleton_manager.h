#ifndef SKELETONMANAGER_H
#define SKELETONMANAGER_H

#include <QWidget>
#include <QDockWidget>

namespace Ui {
class SkeletonManager;
}

class SkeletonManager : public QDockWidget
{
    Q_OBJECT

    public:
        explicit SkeletonManager(QWidget *parent = 0);
        ~SkeletonManager();

    private slots:
        void on_LoadSkeleton_clicked();
        void on_EuclideaDistance_clicked();
        void on_BoneHeat_clicked();
        void on_LBS_clicked();
        void on_DQS_clicked();
        void on_Biharmonic_clicked();
        void on_None_clicked();
        void on_visualizeWeightsBox_clicked(bool checked);
        void on_LoadButton_clicked();

signals:
        void loadSkeleton();
        void activateEuclideanWeights();
        void activateBoneHeatWeights();
        void activateBiharmonicWeights();
        void activateLinearBlendSkinning();
        void activateDualQuaternionSkinning();
        void activateNoneWeights();
        void weightsVisualization(bool b);
        void loadWeights();

private:
        Ui::SkeletonManager *ui;
};

#endif // SKELETONMANAGER_H
