/**
 @author    Marco Livesu (marco.livesu@gmail.com)
 @copyright Marco Livesu 2014.
*/

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "QFileDialog"

// GUI
//
#include "window_manager.h"
#include "trimesh_manager.h"
#include "skeleton_manager.h"
#include "tool_manager.h"
#include "save_manager.h"

// CORE
//
#include "skel/import.h"
#include "skel/export.h"
#include "utils/utils.h"
#include "trimesh/drawable_trimesh.h"

namespace Ui
{
    class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

    public:

        explicit MainWindow(QWidget *parent = 0);
        ~MainWindow();

        void link_gui_to_core();
        void set_render_to_skeleton_visualization();

    private
    slots:
        void add_window_widget_to_dock();
        void add_trimesh_widget_to_dock();
        void add_skeleton_widget_to_dock();
        void add_tool_widget_to_dock();
        void add_save_widget_to_dock();

        void set_full_screen(bool);
        void set_background_color(const QColor &);

        void load_trimesh(const char *);
        void load_skeleton();
        Skel::CurveSkeleton *findSkeleton();
        void set_flat_shading();
        void set_smooth_shading();
        void set_points_shading();
        void set_draw_mesh(bool);
        void set_wireframe(bool);
        void set_wireframe_width(int);
        void set_enable_vertex_color();
        void set_enable_triangle_color();
        void set_vertex_color(const QColor &);
        void set_triangle_color(const QColor &);
        void set_wireframe_color(const QColor &);
        void activate_camera_mode();
        void activate_selection_mode();
        void activate_deselection_mode();
        void activate_deform_mode();
        void activate_none_weights();
        void activate_euclidean_weights();
        void activate_boneheat_weights();
        void activate_biharmonic_weights();
        void activate_lbs_deformation();
        void activate_dqs_deformation();
        void activate_weights_visualization(bool b);
        void saveMesh();
        void saveSkel();
        void saveWeights();
        void loadWeights();


    private:

        // GUI
        //
        Ui::MainWindow  * ui;
        Window_manager  * window_manager;
        Trimesh_manager * trimesh_manager;
        SkeletonManager * skeleton_manager;
        Tool_manager    * tool_manager;
        Save_Manager    * save_manager;

        // Core
        //
        DrawableTrimesh m;
        Skel::CurveSkeleton* skeleton;
};

#endif // MAINWINDOW_H
