/**
 @author    Marco Livesu (marco.livesu@gmail.com)
 @copyright Marco Livesu 2014.
*/

#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent), ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    window_manager  = NULL;
    trimesh_manager = NULL;
    skeleton_manager = NULL;
    tool_manager = NULL;
    save_manager = NULL;

    add_window_widget_to_dock();
    add_trimesh_widget_to_dock();
    add_skeleton_widget_to_dock();
    add_tool_widget_to_dock();
    add_save_widget_to_dock();

    link_gui_to_core();
    showMaximized();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::set_render_to_skeleton_visualization()
{
    //Set flat shading for cage generation
    m.set_wireframe(true);
    m.set_points_shading();

    //Set the new color for wireframe e point shading
    m.set_v_color(1,1,1);
    m.set_wireframe_color(0.8,0.8,0.8);

}

void MainWindow::link_gui_to_core()
{
    connect(ui->actionWindow,   SIGNAL(triggered()),
            this,               SLOT(add_window_widget_to_dock()));

    connect(ui->actionTrimesh,  SIGNAL(triggered()),
            this,               SLOT(add_trimesh_widget_to_dock()));

    connect(window_manager,     SIGNAL(set_full_screen(bool)),
            this,               SLOT  (set_full_screen(bool)));

    connect(window_manager,     SIGNAL(set_background_color(const QColor &)),
            this,               SLOT  (set_background_color(const QColor &)));

    connect(trimesh_manager,    SIGNAL(set_draw_mesh(bool)),
            this,               SLOT  (set_draw_mesh(bool)));

    connect(trimesh_manager,    SIGNAL(load_trimesh(const char *)),
            this,               SLOT  (load_trimesh(const char *)));

    connect(trimesh_manager,    SIGNAL(set_wireframe(bool)),
            this,               SLOT  (set_wireframe(bool)));

    connect(trimesh_manager,    SIGNAL(set_flat_shading()),
            this,               SLOT  (set_flat_shading()));

    connect(trimesh_manager,    SIGNAL(set_smooth_shading()),
            this,               SLOT  (set_smooth_shading()));

    connect(trimesh_manager,    SIGNAL(set_points_shading()),
            this,               SLOT  (set_points_shading()));

    connect(trimesh_manager,    SIGNAL(set_enable_vertex_color()),
            this,               SLOT  (set_enable_vertex_color()));

    connect(trimesh_manager,    SIGNAL(set_enable_triangle_color()),
            this,               SLOT  (set_enable_triangle_color()));

    connect(trimesh_manager,    SIGNAL(set_vertex_color(const QColor &)),
            this,               SLOT  (set_vertex_color(const QColor &)));

    connect(trimesh_manager,    SIGNAL(set_triangle_color(const QColor &)),
            this,               SLOT  (set_triangle_color(const QColor &)));

    connect(trimesh_manager,    SIGNAL(set_wireframe_color(const QColor &)),
            this,               SLOT  (set_wireframe_color(const QColor &)));

    connect(trimesh_manager,    SIGNAL(set_wireframe_width(int)),
            this,               SLOT  (set_wireframe_width(int)));

    connect(skeleton_manager,   SIGNAL(loadSkeleton()),
                                SLOT  (load_skeleton()));

    connect(tool_manager,       SIGNAL(activate_camera_mode()),
            this,               SLOT  (activate_camera_mode()));

    connect(tool_manager,       SIGNAL(activate_selection_mode()),
            this,               SLOT  (activate_selection_mode()));

    connect(tool_manager,       SIGNAL(activate_deselection_mode()),
            this,               SLOT  (activate_deselection_mode()));

    connect(tool_manager,       SIGNAL(activate_deform_mode()),
            this,               SLOT  (activate_deform_mode()));

    connect(skeleton_manager,   SIGNAL(activateNoneWeights()),
            this,               SLOT  (activate_none_weights()));

    connect(skeleton_manager,   SIGNAL(activateEuclideanWeights()),
            this,               SLOT  (activate_euclidean_weights()));

    connect(skeleton_manager,   SIGNAL(activateBoneHeatWeights()),
            this,               SLOT  (activate_boneheat_weights()));

    connect(skeleton_manager,   SIGNAL(activateBiharmonicWeights()),
            this,               SLOT  (activate_biharmonic_weights()));

    connect(skeleton_manager,   SIGNAL(activateLinearBlendSkinning()),
            this,               SLOT  (activate_lbs_deformation()));

    connect(skeleton_manager,   SIGNAL(activateDualQuaternionSkinning()),
            this,               SLOT  (activate_dqs_deformation()));

    connect(skeleton_manager,   SIGNAL(weightsVisualization(bool)),
            this,               SLOT  (activate_weights_visualization(bool)));

    connect(save_manager,       SIGNAL(saveMesh()),
            this,               SLOT  (saveMesh()));

    connect(save_manager,       SIGNAL(saveSkel()),
            this,               SLOT  (saveSkel()));

    connect(save_manager,       SIGNAL(saveWeights()),
            this,               SLOT  (saveWeights()));

    connect(skeleton_manager,   SIGNAL(loadWeights()),
           this,                SLOT  (loadWeights()));
}

/*
 * SLOTS
*/

void MainWindow::load_trimesh(const char *filename)
{
    m = DrawableTrimesh(filename);

    trimesh_manager->deactivateWireframe();
    trimesh_manager->clickSmoothShading();

    ui->glCanvas->clear();
    ui->glCanvas->push_obj(&m);
    ui->glCanvas->fit_scene();
    ui->glCanvas->set_trimesh(&m);
    skeleton_manager->setEnabled(true);
}

void MainWindow::load_skeleton()
{
    std::cout << "Loading Skeleton" << std::endl;

    skeleton = findSkeleton();

    if(skeleton != NULL)
    {
        skeleton->scale(m.getScaleFactor());
        ui->glCanvas->set_skeleton(skeleton);

        trimesh_manager->activateWireframe();
        trimesh_manager->clickPointShading();
    }
}

Skel::CurveSkeleton *MainWindow::findSkeleton()
{

    try
    {
        QString filename = QFileDialog::getOpenFileName(NULL,
                           "Open Skeleton",
                           ".",
                           "SKEL(*.skel *.cg *.cskel)");

        std::cout << "load skeleton: " << filename.toStdString() << std::endl;

        if (!filename.isNull())
        {
            cout << "filename is not null" << endl;
            QStringList parts = filename.split( QChar( '.' ));
                int skel_type = -1;

                if ( parts.last( ).toLower() == "skel" )    { skel_type = 0; } // livesu2012
                if ( parts.last( ).toLower() == "cg" )      { skel_type = 1; } // tagliasacchi2012
                if ( parts.last( ).toLower() == "cskel" )   { skel_type = 2; } // dey&sun

                switch ( skel_type ) {
                case 0:
                    return Skel::Importer::load_TVCG_2012( filename );
                    break;
                case 1:
                    return Skel::Importer::load_Tagliasacchi_2012( filename );
                    break;
                case 2:
                    return Skel::Importer::load_DeySun_2006( filename );
                    break;
                default:
                    throw(std::domain_error("File Format not Supported"));
                    break;
                }
            //OLD //return Skel::Importer::load_TVCG_2012(filename.toStdString().c_str());
        }
        else
        {
            return NULL;
        }
    }
    catch(exception e)
    {
        cout << e.what() << endl;
    }


    return NULL;

}

void MainWindow::set_full_screen(bool b)
{
    ui->glCanvas->setFullScreen(b);
}

void MainWindow::set_wireframe(bool b)
{
    m.set_wireframe(b);
    ui->glCanvas->updateGL();
}

void MainWindow::set_flat_shading()
{
    m.set_flat_shading();
    ui->glCanvas->updateGL();
}

void MainWindow::set_smooth_shading()
{
    m.set_smooth_shading();
    ui->glCanvas->updateGL();
}

void MainWindow::set_points_shading()
{
    m.set_points_shading();
    ui->glCanvas->updateGL();
}

void MainWindow::set_background_color(const QColor & color)
{
    ui->glCanvas->set_clear_color(color);
}

void MainWindow::set_vertex_color(const QColor & color)
{
    m.set_v_color(color.redF(), color.greenF(), color.blueF());
    ui->glCanvas->updateGL();
}

void MainWindow::set_triangle_color(const QColor & color)
{
    m.set_t_color(color.redF(), color.greenF(), color.blueF());
    ui->glCanvas->updateGL();
}

void MainWindow::set_wireframe_color(const QColor & color)
{
    m.set_wireframe_color(color.redF(), color.greenF(), color.blueF());
    ui->glCanvas->updateGL();
}

void MainWindow::activate_camera_mode()
{
    ui->glCanvas->setOperationMode(OperationMode::NONE);
}

void MainWindow::activate_selection_mode()
{
    ui->glCanvas->setOperationMode(OperationMode::ADD);
}

void MainWindow::activate_deselection_mode()
{
    ui->glCanvas->setOperationMode(OperationMode::REMOVE);
}

void MainWindow::activate_deform_mode()
{
    ui->glCanvas->setOperationMode(OperationMode::DEFORM);
}

void MainWindow::activate_none_weights()
{
    ui->glCanvas->setWeightsMode(WeightsMode::NOWEIGHTS);
}

void MainWindow::activate_euclidean_weights()
{
    ui->glCanvas->setWeightsMode(WeightsMode::EUCLIDEAN);
}

void MainWindow::activate_boneheat_weights()
{
    ui->glCanvas->setWeightsMode(WeightsMode::BONEHEAT);
}

void MainWindow::activate_biharmonic_weights()
{
    ui->glCanvas->setWeightsMode(WeightsMode::BIHARMONIC);
}

void MainWindow::activate_lbs_deformation()
{
    ui->glCanvas->setDeformationMode(DeformationMode::LBS);
}

void MainWindow::activate_dqs_deformation()
{
    ui->glCanvas->setDeformationMode(DeformationMode::DQS);
}

void MainWindow::activate_weights_visualization(bool b)
{
    if(b)
    {
        ui->glCanvas->computeInfluence();
        m.set_influence_shading();
        trimesh_manager->deactivateWireframe();
        trimesh_manager->clickSmoothShading();
    }
    else
    {
        m.set_enable_triangle_color();
        trimesh_manager->activateWireframe();
        trimesh_manager->clickPointShading();
    }

    ui->glCanvas->setWVisualization(b);
    ui->glCanvas->updateGL();
}

void MainWindow::saveMesh()
{
    QString filename = QFileDialog::getSaveFileName(NULL,
                       "Save Trimesh",
                       ".",
                       "OBJ(*.obj)");

    std::cout << "save: " << filename.toStdString() << std::endl;

    if (!filename.isEmpty())
    {
        std::string fName = filename.toStdString();
        fName+=".obj";
        m.save(fName.c_str());
    }
}

void MainWindow::saveSkel()
{
    QString filename = QFileDialog::getSaveFileName(NULL,
                       "Save Skeleton",
                       ".",
                       "SKEL(*.skel)");

    std::cout << "save: " << filename.toStdString() << std::endl;

    if (!filename.isEmpty())
    {
        std::string fName = filename.toStdString();
        fName+=".skel";
        Skel::Exporter::SaveToFile(skeleton, QString(fName.c_str()), m.getScaleFactor());
    }
}

void MainWindow::saveWeights()
{
    QString filename = QFileDialog::getSaveFileName(NULL,
                       "Save Weights",
                       ".",
                       "weights(*.weights)");

    std::cout << "save: " << filename.toStdString() << std::endl;

    if (!filename.isEmpty())
    {
        std::string fName = filename.toStdString();
        fName+=".weights";
        ui->glCanvas->saveWeights(fName.c_str());
    }
}

void MainWindow::loadWeights()
{
    QString filename = QFileDialog::getOpenFileName(NULL,
                       "Load Weights",
                       ".",
                       "weights(*.weights)");

    std::cout << "load: " << filename.toStdString() << std::endl;

    if (!filename.isEmpty())
    {
//        std::string fName = filename.toStdString();
//        fName+=".weights";
//        ui->glCanvas->saveWeights(fName.c_str());

        ui->glCanvas->loadWeights(filename.toStdString().c_str());
    }
}

void MainWindow::set_enable_vertex_color()
{
    m.set_enable_vertex_color();
    ui->glCanvas->updateGL();
}

void MainWindow::set_enable_triangle_color()
{
    m.set_enable_triangle_color();
    ui->glCanvas->updateGL();
}

void MainWindow::set_wireframe_width(int width)
{
    m.set_wireframe_width(width);
    ui->glCanvas->updateGL();
}

void MainWindow::set_draw_mesh(bool b)
{
    m.set_draw_mesh(b);
    ui->glCanvas->updateGL();
}

void MainWindow::add_window_widget_to_dock()
{
    if (!window_manager)
    {
        window_manager = new Window_manager(this);
    }
    window_manager->show();
    addDockWidget(Qt::RightDockWidgetArea, window_manager);
}

void MainWindow::add_trimesh_widget_to_dock()
{
    if (!trimesh_manager)
    {
        trimesh_manager = new Trimesh_manager(this);
    }
    trimesh_manager->show();
    addDockWidget(Qt::RightDockWidgetArea, trimesh_manager);
}

void MainWindow::add_skeleton_widget_to_dock()
{
    if (!skeleton_manager)
    {
        skeleton_manager = new SkeletonManager(this);
    }
    skeleton_manager->show();
    addDockWidget(Qt::RightDockWidgetArea, skeleton_manager);
}

void MainWindow::add_tool_widget_to_dock()
{
    if (!tool_manager)
    {
        tool_manager = new Tool_manager(this);
    }
    tool_manager->show();
    addDockWidget(Qt::LeftDockWidgetArea, tool_manager);
}

void MainWindow::add_save_widget_to_dock()
{
    if (!save_manager)
    {
        save_manager = new Save_Manager(this);
    }
    save_manager->show();
    addDockWidget(Qt::RightDockWidgetArea, save_manager);
}
