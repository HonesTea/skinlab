#ifndef TOOL_MANAGER_H
#define TOOL_MANAGER_H

#include <QWidget>
#include <QDockWidget>

namespace Ui {
class Tool_manager;
}

class Tool_manager : public QDockWidget
{
    Q_OBJECT

public:
    explicit Tool_manager(QWidget *parent = 0);
    ~Tool_manager();

private slots:
    void on_deselectButton_clicked();
    void on_deformButton_clicked();
    void on_selectButton_clicked();
    void on_cameraButton_clicked();

signals:
    void activate_camera_mode();
    void activate_selection_mode();
    void activate_deselection_mode();
    void activate_deform_mode();

private:
    Ui::Tool_manager *ui;

    void activate_camera();
    void activate_selection();
    void activate_deselection();
    void activate_deform();
};

#endif // TOOL_MANAGER_H
