/**
 @author    Marco Livesu (marco.livesu@gmail.com)
 @copyright Marco Livesu 2014.
*/

#include "glcanvas.h"

GLcanvas::GLcanvas(QWidget * parent)
{
    setParent(parent);

    isSkelLoaded = false;
    isRectangleActive = false;
    weightsVisualization = false;
    setOperationMode(NONE);
    currentConstrainedAxis = NONE_CONSTR_AXIS;

    clear_color = Qt::white;
}

void GLcanvas::init()
{
    setFPSIsDisplayed(true);
//    camera()->frame()->setSpinningSensitivity(100.0);

    // A DeformationFrameConstraint will apply displacements to the selection
    setManipulatedFrame(new qglviewer::ManipulatedFrame());
    deformationFrameConstraint = new DeformationFrameConstraint();
    manipulatedFrame()->setConstraint(deformationFrameConstraint);
    manipulatedFrame()->setSpinningSensitivity(100.0);
}

void GLcanvas::clear()
{
    drawlist.clear();
    skeleton = NULL;
}

void GLcanvas::draw()
{
    setBackgroundColor(clear_color);

    for(int i=0; i<(int)drawlist.size(); ++i)
    {
        drawlist[i]->draw();
    }

    if (isRectangleActive)
      drawSelectionRectangle();

    glPushMatrix();
    glMultMatrixd(manipulatedFrame()->matrix());
    glPopMatrix();


    if(isSkelLoaded)
    {
        Skel::Draw::drawGL(skeleton, Skel::Draw::FEATURE_POINTS | Skel::Draw::BONES | Skel::Draw::BONE_POINTS, mesh->scene_radius() / 200);
    }
}

int GLcanvas::push_obj(const DrawableObject * obj)
{
    drawlist.push_back(obj);
    updateGL();

    return drawlist.size();
}

int GLcanvas::set_skeleton(const Skel::CurveSkeleton *s)
{
    skeleton = (Skel::CurveSkeleton *) s;

    selection_.clear();
    deformationFrameConstraint->clearSet();
    deformationFrameConstraint->setSkel(skeleton);
    isSkelLoaded = true;
    setWeightsMode(WeightsMode::NOWEIGHTS);
    boneHeatCalculated = false;
    biharmonicCalculated = false;
    computeInfluence();
    updateGL();
    return 1;
}

void GLcanvas::set_trimesh(const DrawableTrimesh* trimesh)
{
    this->mesh = (DrawableTrimesh*) trimesh;
    deformationFrameConstraint->clearSet();
    deformationFrameConstraint->setTrimesh(mesh);
}

void GLcanvas::saveWeights(const char *filename)
{
    deformationFrameConstraint->saveWeights(filename);
}

void GLcanvas::loadWeights(const char *filename)
{
    deformationFrameConstraint->loadWeights(filename);
}

void GLcanvas::computeInfluence()
{
    if(isSkelLoaded && (selection_.size()>0))
    {
        Eigen::SparseMatrix<double> weights = deformationFrameConstraint->getWeights(modeW);
        vector<float> texPos(mesh->num_vertices());

        for(int i =0; i < mesh->num_vertices(); ++i)
        {
            for(int j =0; j < selection_.size(); ++j)
            {
                texPos[i] += (weights.coeffRef(i,selection_.at(j)));
            }
        }

        for (int i=0; i <mesh->num_vertices(); ++i)
        {
            if(texPos[i] > 0.99)
            {
                texPos[i] = 0.99;
            }
        }

        mesh->set_texture_pos(texPos);
    }
    else
    {
        vector<float> texPos(mesh->num_vertices());

        mesh->set_texture_pos(texPos);
    }
    updateGL();
}

void GLcanvas::setWVisualization(bool b)
{
    weightsVisualization = b;
}

void GLcanvas::fit_scene()
{
    vec3d center(0,0,0);
    float radius = 0.0;
    int   count  = 0;

    for(int i=0; i<(int)drawlist.size(); ++i)
    {
        const DrawableObject * obj = drawlist[i];

        center += obj->scene_center();
        radius  = max(radius, obj->scene_radius());
        ++count;
    }

    center /= (double)count;

    setSceneCenter(qglviewer::Vec(center.x(), center.y(), center.z()));
    setSceneRadius(radius);

    showEntireScene();
}

void GLcanvas::set_clear_color(const QColor &color)
{
    clear_color = color;
    updateGL();
}

void GLcanvas::mousePressEvent(QMouseEvent* e)
{
    rectangle_ = QRect(e->pos(), e->pos());

    if ((modeO == ADD) || (modeO == REMOVE))
    {
        isRectangleActive = true;
        rectangle_ = QRect(e->pos(), e->pos());
    }
    else if ((modeO == DEFORM) && (selection_.size()>0))
    {
        startManipulation();
    }

    QGLViewer::mousePressEvent(e);
}

void GLcanvas::mouseMoveEvent(QMouseEvent* e)
{
    if (isRectangleActive)
    {
      // Updates rectangle_ coordinates and redraws rectangle
      rectangle_.setBottomRight(e->pos());
      updateGL();
    }

    QGLViewer::mouseMoveEvent(e);

}

void GLcanvas::mouseReleaseEvent(QMouseEvent* e)
{
    if (isRectangleActive)
    {
        // Actual selection on the rectangular area.
        // Possibly swap left/right and top/bottom to make rectangle_ valid.
        rectangle_ = rectangle_.normalized();

        // Define selection window dimensions
        int width = rectangle_.width();
        if(width>1)
            setSelectRegionWidth(width);
        else
            setSelectRegionWidth(1);

        int height = rectangle_.height();
        if(height>1)
            setSelectRegionHeight(rectangle_.height());
        else
            setSelectRegionHeight(1);

        // Compute rectangle center and perform selection
        select(rectangle_.center());
        // Update display to show new selected objects
        isRectangleActive = false;
        updateGL();
    }
    updateGL();

    QGLViewer::mouseReleaseEvent(e);
}

void GLcanvas::endSelection(const QPoint&)
{
    // Flush GL buffers
    glFlush();
    // Get the number of objects that were seen through the pick matrix frustum. Reset GL_RENDER mode.
    GLint nbHits = glRenderMode(GL_RENDER);

    if (nbHits > 0)
    {
        // Interpret results : each object created 4 values in the selectBuffer().
        // (selectBuffer())[4*i+3] is the id pushed on the stack.
        for (int i=0; i<nbHits; ++i)
        {
            switch (modeO)
            {
                case ADD    :
                    addIdToSelection((selectBuffer())[4*i+3]);
                    break;
                case REMOVE :
                    removeIdFromSelection((selectBuffer())[4*i+3]);
                    break;
                default :
                    break;
            }
        }

        if(weightsVisualization)
        {
            computeInfluence();
        }
    }
}

void GLcanvas::drawWithNames()
{
    if(isSkelLoaded)
    {
        for (int i = 0; i < skeleton->points.size(); i++)
        {
            glPushName(i);
            Skel::Draw::drawPointGL(skeleton->points.at(i), 1.0);
            glPopName();
        }
    }
}

//this method will look for the nearest handle to the articulation
//this is done because the used skeleton structure have no hierarchy
void GLcanvas::setDeformationFramePosition()
{
    Vec rotationCenterPosition;
    int distanceFromArticulation;
    int newDistance;
    int closest;
    int pivot = -1;

    /*distanceFromArticulation = skeleton->distanceFromArticulation(selection_.at(0));
    closest = selection_[0];

    for(int i = 0; i < selection_.size(); i++)
    {
        newDistance = skeleton->distanceFromArticulation(selection_.at(i));

        if ((distanceFromArticulation > newDistance) && (newDistance > -1))
        {
            closest = selection_.at(i);
            distanceFromArticulation = newDistance;
        }
    }*/

//    int i = 0;
//    while(pivot == -1 || i < selection_.size())
//    {
//        Skel::SkelPoint p = skeleton->points.at(selection_.at(i));

//        for(int j = 0; j < p.neighbors.size(); ++j)
//        {
//            if(!selection_.contains(p.neighbors[j]))
//            {
//                pivot = selection_[i];
//            }
//        }

//        ++i;
//    }

    pivot = findPivot();

    rotationCenterPosition = Vec(skeleton->points.at(pivot).coord.x,
                                 skeleton->points.at(pivot).coord.y,
                                 skeleton->points.at(pivot).coord.z);

    manipulatedFrame()->setPosition(rotationCenterPosition);

    deformationFrameConstraint->setHandleID(pivot);
}

int GLcanvas::findPivot()
{
    int i = 0;
    while(i < selection_.size())
    {
        Skel::SkelPoint p = skeleton->points.at(selection_.at(i));

        for(int j = 0; j < p.neighbors.size(); ++j)
        {
            if(!selection_.contains(p.neighbors[j]))
            {
                return selection_[i];
            }
        }

        ++i;
    }

    return selection_[0];
}

void GLcanvas::startManipulation()
{
    DeformationFrameConstraint* dfc = (DeformationFrameConstraint*)(manipulatedFrame()->constraint());
    dfc->clearSet();

    // Add all selected points to the list of points that will be manipulated.
    for (int i = 0; i < selection_.size(); i++)
    {
        dfc->addObjectToSet(selection_[i]);
    }

    //the nearest point to the articulation will be chosen to be the center of manipulation
    setDeformationFramePosition();
}

void GLcanvas::addIdToSelection(int id)
{
    if (!selection_.contains(id))
    {
        selection_.push_back(id);
        skeleton->points[id].select();
    }
}

void GLcanvas::removeIdFromSelection(int id)
{
    selection_.removeAll(id);
    skeleton->points[id].deselect();
}

void GLcanvas::drawSelectionRectangle() const
{
    startScreenCoordinatesSystem();
    glDisable(GL_LIGHTING);
    glEnable(GL_BLEND);

    glDisable(GL_BLEND);

    glLineWidth(2.0);
    glColor4f(0.7f, 0.7f, 0.8f, 0.5f);
    glBegin(GL_LINE_LOOP);
    glVertex2i(rectangle_.left(),  rectangle_.top());
    glVertex2i(rectangle_.right(), rectangle_.top());
    glVertex2i(rectangle_.right(), rectangle_.bottom());
    glVertex2i(rectangle_.left(),  rectangle_.bottom());
    glEnd();

    glEnable(GL_LIGHTING);
    stopScreenCoordinatesSystem();
}


void GLcanvas::setOperationMode(OperationMode _mode)
{
    modeO = _mode;

    switch(modeO)
    {
        case NONE:
            setMouseBinding(Qt::NoModifier, Qt::LeftButton, CAMERA, ROTATE);
            setMouseBinding(Qt::NoModifier, Qt::RightButton, CAMERA, TRANSLATE);
            displayMessage("CAMERA MODE ACTIVATED", 2000);
            break;

        case ADD:
            setMouseBinding(Qt::NoModifier, Qt::LeftButton, NO_CLICK_ACTION, false, Qt::NoButton);
            setMouseBinding(Qt::NoModifier, Qt::RightButton, NO_CLICK_ACTION, false, Qt::NoButton);
            displayMessage("SELECTION MODE ACTIVATED", 2000);
            break;

        case REMOVE:
            setMouseBinding(Qt::NoModifier, Qt::LeftButton, NO_CLICK_ACTION, false, Qt::NoButton);
            setMouseBinding(Qt::NoModifier, Qt::RightButton, NO_CLICK_ACTION, false, Qt::NoButton);
            displayMessage("DESELECTION MODE ACTIVATED", 2000);
            break;

        case DEFORM:
            setMouseBinding(Qt::NoModifier, Qt::LeftButton, FRAME, ROTATE);
            setMouseBinding(Qt::NoModifier, Qt::RightButton, FRAME, TRANSLATE);
            displayMessage("DEFORMATION MODE ACTIVATED", 2000);
            break;
    }
}

void GLcanvas::setWeightsMode(WeightsMode _mode)
{
    modeW = _mode;

    switch(modeW)
    {
        case WeightsMode::BONEHEAT:
        {
            if(!boneHeatCalculated)
            {
                boneHeatCalculated = true;
                displayMessage("CALCULATING BONEHEAT WEIGHTS", 200000000);
            }
            break;
        }
        case WeightsMode::BIHARMONIC:
        {
            if(!biharmonicCalculated)
            {
                biharmonicCalculated = true;
                displayMessage("CALCULATING BIHARMONIC WEIGHTS", 200000000);
            }
            break;
        }
        default: break;
    }
    updateGL();

    deformationFrameConstraint->setWeightMode(modeW);

    displayMessage("WEIGHTS CALCULATED", 2000);

    switch(modeW)
    {
        case WeightsMode::BONEHEAT:
        {
            displayMessage("BONEHEAT WEIGHTS SELECTED", 2000);
            break;
        }
        case WeightsMode::BIHARMONIC:
        {
            displayMessage("BIHARMONIC WEIGHTS SELECTED", 2000);
            break;
        }
    case WeightsMode::EUCLIDEAN:
        {
            displayMessage("RIGID WEIGHTS SELECTED", 2000);
            break;
        }
        default:displayMessage("NO WEIGHTS SELECTED", 2000);
        break;
    }
    updateGL();

    if(weightsVisualization)
    {
        computeInfluence();
    }
}

void GLcanvas::setDeformationMode(DeformationMode _mode)
{
    deformationFrameConstraint->setDeformMode(_mode);
}

