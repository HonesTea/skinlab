#ifndef TETMESH_H
#define TETMESH_H

#include <vector>
#include <algorithm>

using namespace std;


class TetMesh
{
public:
    TetMesh();

    void            init();

    bool            addFace            (int v1, int v2, int v3);
    void            addEdge            (int v1, int v2);

    void            setVertex          (int i, double x, double y, double z);
    void            setCoord           (int i, double _coord);
    void            setFace            (int i, int v1, int v2, int v3);
    void            setTetrahedron     (int i, int v1, int v2, int v3, int v4);
    vector<double>  getVertex          (int i);
    double          getCoord           (int i);
    vector<int>     getEdge            (int i);
    vector<int>     getFace            (int i);
    vector<int>     getTetrahedron     (int i);

    void            setNumCoords       (int n);
    void            setNumEdges        (int n);
    void            setNumFaces        (int n);
    void            setNumTetrahedron  (int n);
    int             getNumCoords       ();
    int             getNumEdges        ();
    int             getNumFaces        ();
    int             getNumTetrahedron  ();

    vector<double>  getCoords          ();
    vector<int>     getFaces           ();
    vector<int>     getEdges           ();
    vector<int>     getTetrahedrons    ();

private:
    vector<double>          coords;
    vector<int>             edges;
    vector<int>             faces;
    vector<int>             tetrahedron;

    int                     numCoords;
    int                     numEdges;
    int                     numFaces;
    int                     numTetrahedron;
};

#endif // TETMESH_H
