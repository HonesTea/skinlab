#include "tetmesh.h"

TetMesh::TetMesh()
{
    numCoords = 0;
    numEdges = 0;
    numFaces = 0;
    numTetrahedron = 0;
}

bool TetMesh::addFace(int v1, int v2, int v3)
{
    int fid = 0;
    while(fid < faces.size())
    {
        if(faces[fid] == v1 || faces[fid] == v2 || faces[fid] == v3)
        {
            if(faces[fid +1] == v1 || faces[fid +1] == v2 || faces[fid +1] == v3)
            {
                if(faces[fid +2] == v1 || faces[fid +2] == v2 || faces[fid +2] == v3)
                {
                    return false;
                }
            }
        }

        fid += 3;
    }

    faces.push_back(v1);
    faces.push_back(v2);
    faces.push_back(v3);

    return true;
}

void TetMesh::addEdge(int v1, int v2)
{
    edges.push_back(v1);
    edges.push_back(v2);
}

void TetMesh::setVertex(int i, double x, double y, double z)
{
    coords[i*3   ] = x;
    coords[i*3 +1] = y;
    coords[i*3 +2] = z;
}

void TetMesh::setCoord(int i, double _coord)
{
    coords[i] = _coord;
}

void TetMesh::setFace(int i, int v1, int v2, int v3)
{
    faces[i*3   ] = v1;
    faces[i*3 +1] = v2;
    faces[i*3 +2] = v3;
}

void TetMesh::setTetrahedron(int i, int v1, int v2, int v3, int v4)
{
    tetrahedron[i*4   ] = v1;
    tetrahedron[i*4 +1] = v2;
    tetrahedron[i*4 +2] = v3;
    tetrahedron[i*4 +3] = v4;

}

vector<double> TetMesh::getVertex(int i)
{
    vector<double> v = {coords[i*3   ],
                        coords[i*3 +1],
                        coords[i*3 +2]};

    return v;
}

double TetMesh::getCoord(int i)
{
    return coords[i];
}

vector<int> TetMesh::getEdge(int i)
{
    vector<int> e = {edges[i*2], edges[i*2 +1]};

    return e;
}

vector<int> TetMesh::getFace(int i)
{
    vector<int> f =    {faces[i*3   ],
                        faces[i*3 +1],
                        faces[i*3 +2]};

    return f;
}

vector<int> TetMesh::getTetrahedron(int i)
{
    vector<int> tet =    {tetrahedron[i*4   ],
                          tetrahedron[i*4 +1],
                          tetrahedron[i*4 +2],
                          tetrahedron[i*4 +3]};

    return tet;
}

void TetMesh::setNumCoords(int n)
{
    numCoords = n;
    coords.resize(n);
}

void TetMesh::setNumEdges(int n)
{
    numEdges = n;
//    edges.resize(n *2);
}

void TetMesh::setNumFaces(int n)
{
    numFaces = n;
    faces.resize(n *3);
}

void TetMesh::setNumTetrahedron(int n)
{
    numTetrahedron = n;
    tetrahedron.resize(n *4);
}

int TetMesh::getNumCoords()
{
    return numCoords;
}

int TetMesh::getNumEdges()
{
    return numEdges;
}

int TetMesh::getNumFaces()
{
    return faces.size() /3;
}

int TetMesh::getNumTetrahedron()
{
    return numTetrahedron;
}

vector<double> TetMesh::getCoords()
{
    return coords;
}

vector<int> TetMesh::getFaces()
{
    return faces;
}

vector<int > TetMesh::getEdges()
{
    return edges;
}

vector<int> TetMesh::getTetrahedrons()
{
    return tetrahedron;
}


