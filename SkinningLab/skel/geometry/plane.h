#ifndef PLANE_H
#define PLANE_H

#include "vec3.h"
#include "Eigen/Geometry"
#include "Eigen/Core"

class Plane
{
public:
                Plane ();
                Plane ( const vec3d& n_, const vec3d& p_ );
    void        set_plane ( const vec3d& n_, const vec3d& p_ );
    double      get_point_position ( const vec3d& p_ );
    bool        do_intersect_with_line ( const vec3d& v1, const vec3d& v2, vec3d& intersection );
    bool        compute_intersection_with_line ( const vec3d& v1, const vec3d& v2, vec3d& intersection );

private:
    double      d;
    vec3d       n;
    bool        is_plane_initialized;
};

#endif // PLANE_H
