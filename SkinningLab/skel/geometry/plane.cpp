#include "geometry/plane.h"

Plane::Plane ()
{
    is_plane_initialized = false;
}

Plane::Plane ( const vec3d& n_, const vec3d& p_ )
{
    set_plane(n_,p_);
}

void Plane::set_plane ( const vec3d& n_, const vec3d& p_ )
{
    n = n_;
    d = p_.dot(n);
    is_plane_initialized = true;
}

double Plane::get_point_position ( const vec3d& p_ )
{
    if (is_plane_initialized)
    {
        return p_.dot(n)-d;
    }
    else
    {
        return -1.0; //Da modificare con un assert
    }
}

bool Plane::do_intersect_with_line ( const vec3d& v1, const vec3d& v2, vec3d& intersection )
{
    double v1_side = get_point_position(v1);
    double v2_side = get_point_position(v2);

    //if the two end-vertices are located on different side of the main plane
    if(v1_side*v2_side<0)
    {
        //compute the intersection point
        compute_intersection_with_line(v1, v2, intersection);
        return true;
    }
    return false;
}

bool Plane::compute_intersection_with_line( const vec3d& v1, const vec3d& v2, vec3d& intersection )
{
    vec3d dir = v2-v1;
    double u = dir[0];
    double v = dir[1];
    double w = dir[2];

    Eigen::Matrix3d A;
    A(0,0) = n[0];     A(0,1) = n[1];     A(0,2) = n[2];
    A(1,0) = w;        A(1,1) = 0;        A(1,2) = -u;
    A(2,0) = v;        A(2,1) = -u;       A(2,2) = 0;

    Eigen::Vector3d b;
    b(0) = d;
    b(1) = (w*v1[0])-(u*v1[2]);
    b(2) = (v*v1[0])-(u*v1[1]);

    Eigen::Vector3d x = A.colPivHouseholderQr().solve(b);

    intersection = vec3d(x[0], x[1], x[2]);

    return true;
}
