/****************************************************************************
 Copyright (C) 2002-2013 Gilles Debunne. All rights reserved.
 This file is part of the QGLViewer library version 2.5.2.
 http://www.libqglviewer.com - contact@libqglviewer.com
 This file may be used under the terms of the GNU General Public License
 versions 2.0 or 3.0 as published by the Free Software Foundation and
 appearing in the LICENSE file included in the packaging of this file.
 In addition, as a special exception, Gilles Debunne gives you certain
 additional rights, described in the file GPL_EXCEPTION in this package.
 libQGLViewer uses dual licensing. Commercial/proprietary software must
 purchase a libQGLViewer Commercial License.
 This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
 WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
*****************************************************************************/

#include "QGLViewer/constraint.h"
#include "QGLViewer/frame.h"
#include "skel/import.h"
#include "skel/skelpoint/base.h"
#include "utils/utils.h"
#include "trimesh/drawable_trimesh.h"
#include "Skinning/linearblendskinning.h"
#include "Skinning/skinningmanager.h"
#include "Eigen/Dense"

using namespace qglviewer;

class DeformationFrameConstraint : public LocalConstraint
{
public:

    DeformationFrameConstraint();
    void clearSet();
    void addObjectToSet(int vId);
    void setHandleID(int id);
    void setSkel(Skel::CurveSkeleton *s);
    void setTrimesh(DrawableTrimesh *m);
    void setDirectionConstraint(Axis axisId);
    void setWeightMode(WeightsMode _mode);
    void setDeformMode(DeformationMode _mode);

    Eigen::SparseMatrix<double> getWeights (WeightsMode _mode);
    void saveWeights(const char* filename);
    void loadWeights(const char *filename);

    virtual void constrainTranslation(qglviewer::Vec &translation, Frame *const frame);
    virtual void constrainRotation(qglviewer::Quaternion &rotation, Frame *const frame);

private:

    Eigen::Matrix4d getHomogeneousTranslationMatrix(double x, double y, double z);
    Eigen::Matrix4d getHomogeneousRotationMatrixFromQuaternion (Quaternion q);


    QList<int>          objects_;
    Skel::CurveSkeleton *skel;
    DrawableTrimesh     *mesh;
    SkinningManager     skinner;
    int handleId;

    Vec                 constrainedAxis;
    bool                isAxisContraintEnabled;
};
