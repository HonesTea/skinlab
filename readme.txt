SkinLab is a tool utilized to produce skinning deformations on 3D model. 
It is produced as thesis by Alessandro Mereu, which the it is avaible at the following link https://bitbucket.org/HonesTea/skinlab_thesis/overview

In SkinLab there are implemented the Dual Quaternion skinning alghorithm, and three methods to calculate skinning Weights.
Rigid, BoneHeat diffusion and Bounded biharmonic (as explained in http://skinning.org/ by Alec Jacobson et al. People far better than me.)
Inside the folder TestModels there are some models with the relative skeletons and weights already calculated.

This work is under development, far away from being completed or fully funtional.

The software use vary libraries and third party tools that need to be downloaded, licensed and installed in order to compile it.
You can find the needed documentation for all of them in their own sites.

http://libqglviewer.com/
http://wias-berlin.de/software/tetgen/
http://www.gurobi.com/

Plus, the code use others libraries like Eigen, the skeleton data structure developed by Francesco Usai and the trimesh data structure developed by Marco Livesu.